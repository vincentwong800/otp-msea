// tests.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "tests.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 唯一的应用程序对象

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// 初始化 MFC 并在失败时显示错误
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 更改错误代码以符合您的需要
		_tprintf(_T("错误: MFC 初始化失败\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 在此处为应用程序的行为编写代码。
		HMODULE hotp = ::LoadLibrary(TEXT("otp.dll"));

		if (hotp)
		{
			typedef string (__stdcall *fnGETOPT)(string &secrect);
			fnGETOPT fnGetOtp = (fnGETOPT)GetProcAddress(hotp, "get_otp");
			if (fnGetOtp)
			{
				string secrect = "GIZTQOBSGBCDMMKFIE2EENBZ";
				if (argc == 2)
				{
					TString ts1 = argv[1];
					String::wstring2string(ts1, secrect);
				}
				string res = fnGetOtp(secrect);
				Output::DbgPrintA("%s,%s", secrect.c_str(), res.c_str());
			}
		}

		time_t t;
		time(&t);
		//DbgPrintA("t:%d", t);
	}

	return nRetCode;
}
