#ifndef _COMMON_HEADER_
#define _COMMON_HEADER_

#pragma data_seg(".d3")

//////////////////////////////////////////////////////////////////////////
// 忽略一些警告信息

#if (_MSC_VER >= 800)
#pragma warning(disable:4514)
#ifndef __WINDOWS_DONT_DISABLE_PRAGMA_PACK_WARNING__
#pragma warning(disable:4103)
#endif

#if _MSC_VER >= 1200
//#pragma warning(push)
#endif
#pragma warning(disable:4001)
#pragma warning(disable:4201)
#pragma warning(disable:4214)

#pragma warning(disable:4089) // all references to "xxx.dll" discarded by /OPT:REF
#pragma warning(disable:4101) // 未引用的局部变量
#pragma warning(disable:4102) // 未引用的标签
#pragma warning(disable:4311) // 'variable' : pointer truncation from 'type' to 'type'
#pragma warning(disable:4312) // 'operation' : conversion from 'type1' to 'type2' of greater size
#pragma warning(disable:4414) // short jump to function converted to near
#pragma warning(disable:4731) // 框架指针寄存器“ebp”被内联程序集代码修改
#pragma warning(disable:4733) // 内联 asm 分配到“FS:0”: 处理程序未注册为安全处理程序
#pragma warning(disable:4748) // 由于函数中禁用了优化功能，/GS 不能防止参数和局部变量造成本地缓冲区溢出
//#pragma warning(disable:4819) // 该文件包含不能在当前代码页(936)中表示的字符。请将该文件保存为 Unicode 格式以防止数据丢失
#pragma warning(disable:4995) // 名称被标记为 #pragma deprecated
#pragma warning(disable:4996) // 被声明为已否决

#endif

#if _MSC_VER >= 1300 //vs2002以上
#define DEVELOPMENT_PLATFORM_VS
#pragma warning(disable:1684) // conversion from pointer to same-sized integral type (potential portability problem)
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
//#define _WIN32_WINNT 0x0501
#include <WINDOWS.H>
#ifdef DEVELOPMENT_PLATFORM_VS
#include <WindowsX.h>
#endif

#include <tchar.h>
//#include <ntstatus.h>
#include <tlhelp32.h>
#include <process.h>

#if _MSC_VER >= 1800 //vs2013以上
#include <VersionHelpers.h>
#else //vs2013以下
#ifndef nullptr
#define nullptr NULL
#endif
#endif

//////////////////////////////////////////////////////////////////////////
#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)

// __WFILE__: __FILE__的宽字符形式
#define __WFILE__ WIDEN(__FILE__)

//////////////////////////////////////////////////////////////////////////
// 宏 连接2个标记符成为字符串，如CAT(hello,world)，结果"helloworld"
#define CAT2(a) #a
#define CAT(a,b) CAT2(a##b)

#define WCAT2(a) L#a
#define WCAT(a,b) WCAT2(a##b)


#ifdef UNICODE
#define TFILE	__WFILE__
#else
#define TFILE	__FILE__
#endif

//////////////////////////////////////////////////////////////////////////
// 代码标记输出
#define chSTR2(x)		#x
#define chSTR(x)		chSTR2(x)
#define chMSG(desc)		message(__FILE__ "(" chSTR(__LINE__) "):" #desc)

//#include <iostream>
//#include <typeinfo>
//#include <iomanip>

#ifdef DEVELOPMENT_PLATFORM_VS 

#define FunctionA __FUNCTION__
#define FunctionW WIDEN(__FUNCTION__)
#ifdef UNICODE
#define TFunction FunctionW
#else
#define TFunction FunctionA
#endif

#define FunctionNameA __FUNCDNAME__
#define FunctionNameW WIDEN(__FUNCDNAME__)
#ifdef UNICODE
#define TFunctionName FunctionNameW
#else
#define TFunctionName FunctionNameA
#endif

#define FunctionSigA __FUNCSIG__
#define FunctionSigW WIDEN(__FUNCSIG__)
#ifdef UNICODE
#define TFunctionSig FunctionSigW
#else
#define TFunctionSig FunctionSigA
#endif

//// 函数名字，char* 类型
//#define cFunction		__FUNCTION__
//// 函数修饰名，char* 类型
//#define cFunctionName	__FUNCDNAME__
//// 函数标签，char* 类型
//#define cFunctionSig	__FUNCSIG__

// 查看函数名，编译时就可输出
// 使用格式 #pragma FUNCTION/FUNCTIONNAME/FUNCTIONSIG
#pragma chMSG('#pragma FUNCTIONNAME '显示的函数名可在汇编里面的call使用:call xxx)

#define FUNCTION			message(__FILE__ "(" chSTR(__LINE__) "):" FunctionA)
#define FUNCTIONNAME		message(__FILE__ "(" chSTR(__LINE__) "):" FunctionNameA)
#define FUNCTIONSIG			message(__FILE__ "(" chSTR(__LINE__) "):" FunctionSigA)

// 输出本调用的函数名
#define CALLERNAME()		{char buf[100]={0}; Output::DbgPrintBufferA(buf, 100, "%s", FunctionA);}
#define CALLERNAMEEND()		{char buf[100]={0}; Output::DbgPrintBufferA(buf, 100, "%s end.", FunctionA);}

#else
// 函数名字，char* 类型
#define cFunction			"cfunction"
// 函数修饰名，char* 类型
#define cFunctionName		"cfunctionname"
// 函数标签，char* 类型
#define cFunctionSig		"cfunctionsig"

#define FunctionA cFunction
#define FunctionW WIDEN(cFunction)
#ifdef UNICODE
#define TFunction FunctionW
#else
#define TFunction FunctionA
#endif

#define FunctionNameA cFunctionName
#define FunctionNameW WIDEN(cFunctionName)
#ifdef UNICODE
#define TFunctionName FunctionNameW
#else
#define TFunctionName FunctionNameA
#endif

#define FunctionSigA cFunctionSig
#define FunctionSigW WIDEN(cFunctionSig)
#ifdef UNICODE
#define TFunctionSig FunctionSigW
#else
#define TFunctionSig FunctionSigA
#endif

#endif

#ifdef DEVELOPMENT_PLATFORM_VS
	#ifdef VMPROTECT_DISABLE
	#define VMBEGIN
	#define VMEND
	#define VMBEGIN2(a)
	#define VMBEGINM
	#define VMBEGINV
	#define VMBEGINU
	#define VMSETSN(a)		0
	#define VMSETGH(a,b)	10
	#else
	#define VMBEGIN			VMProtectBegin(FunctionNameA)
	#define VMEND			VMProtectEnd()
	#define VMBEGIN2(a)		VMProtectBegin(a)
	#define VMBEGINM		VMProtectBeginMutation(FunctionNameA)
	#define VMBEGINV		VMProtectBeginVirtualization(FunctionNameA)
	#define VMBEGINU		VMProtectBeginUltra(FunctionNameA)
	#define VMSETSN(a)		VMProtectSetSerialNumber(a)
	#define VMSETGH(a,b)	VMProtectGetCurrentHWID(a,b)
	#endif
#else
	#ifdef VMPROTECT_DISABLE
	#define VMBEGIN
	#define VMBEGINvc6(a)
	#define VMEND
	#define VMBEGIN2(a)
	#define VMBEGINvc6M(a)
	#define VMBEGINvc6V(a)
	#define VMBEGINvc6U(a)
	#define VMSETSN(a)		0
	#else
	#define VMBEGIN			VMProtectBegin(FunctionNameA)
	#define VMBEGINvc6(a)	VMProtectBegin(a)
	#define VMEND			VMProtectEnd()
	#define VMBEGIN2(a)		VMProtectBegin(a)
	#define VMBEGINvc6M(a)	VMProtectBeginMutation(a)
	#define VMBEGINvc6V(a)	VMProtectBeginVirtualization(a)
	#define VMBEGINvc6U(a)	VMProtectBeginUltra(a)
	#define VMSETSN(a)		VMProtectSetSerialNumber(a)
	#endif
#endif

//#include <C/VMProtectSDK.h>
////#include "CmnHdr.h"

#pragma chMSG(--------------------------------------------------)
#ifdef _WIN64
#pragma chMSG(不使用vm加密的，发布要带VMProtectSDK64.dll)
#else
#pragma chMSG(不使用vm加密的，发布要带VMProtectSDK32.dll)
#endif
#pragma chMSG(--------------------------------------------------)

// GetMappedFileName
#include <Psapi.h>
#pragma comment(lib, "psapi.lib")

// StrStr， StrNCat...
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")

//#ifdef DEVELOPMENT_PLATFORM_VS
// detours header
//#include "DetoursPro/detours.h"
//#ifdef _WIN64
//#pragma comment(lib, "f:/wang/myproject/Win32Header/DetoursPro/lib.X64/detours.lib")
//#else
//#pragma comment(lib, "detours.lib")
//#endif

//#else
//#include "detours.h"
//#pragma comment(lib, "detours.lib")
//#endif

//#ifndef _WIN64
//#define ULONG_PTR ULONG
//#endif

#ifdef __AFXWIN_H__ // MFC requires
#pragma chMSG(有些工程包含会出错)
#include <AFXCONV.H>
#include <afxmt.h> //CCriticalSection
#include <AFXTEMPL.H> //CMap

#ifdef GDIPLUS_INCLUDE
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")
//#ifndef _WIN64
//#include "GDIPlus/Includes/GdiPlus.h"
//using namespace Gdiplus;
//#pragma comment(lib, "f:/wang/myproject/Win32Header/GDIPlus/lib/gdiplus.lib")
//#endif
#endif

//////////////////////////////////////////////////////////////////////////
// RichEditCtrl，一行字符串设置多个格式要用到此结构数组
typedef struct tagCHARFORMATRANGE
{
	CHARRANGE cr;
	CHARFORMAT cf;
} CHARFORMATRANGE, *PCHARFORMATRANGE;

#endif  // __AFXWIN_H__


#ifndef VC_EXTRALEAN
#ifndef UNICODE
#include <afxisapi.h>
#endif
#endif //VC_EXTRALEAN

#if !defined(_W64)
#if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) && _MSC_VER >= 1300
#define _W64 __w64
#else
#define _W64
#endif
#endif

#ifndef _UINTPTR_T_DEFINED
#ifdef  _WIN64
typedef unsigned __int64    uintptr_t;
#else
typedef _W64 unsigned int   uintptr_t;
#endif
#define _UINTPTR_T_DEFINED
#endif

//#if defined(_WIN64)
////    typedef __int64 INT_PTR, *PINT_PTR;
////    typedef unsigned __int64 UINT_PTR, *PUINT_PTR;
//#if !defined(LONG_PTR)
//typedef __int64 LONG_PTR, *PLONG_PTR;
//typedef unsigned __int64 ULONG_PTR, *PULONG_PTR;
//#endif
//
////#define __int3264   __int64
//
//#else
////    typedef _W64 int INT_PTR, *PINT_PTR;
////    typedef _W64 unsigned int UINT_PTR, *PUINT_PTR;
//#if !defined(LONG_PTR)
//typedef _W64 LONG LONG_PTR, *PLONG_PTR;
//typedef _W64 ULONG ULONG_PTR, *PULONG_PTR;
//#endif
//
////#define __int3264   __int32
//#endif

#include <string>
#include <algorithm>
#include <vector>
#include <list>
#include <map>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <iostream>
//#include <cctype>
//#include <iterator>
using namespace std;

#include <strsafe.h>

#include <assert.h>
#ifndef ASSERT
#define ASSERT			assert
#endif

// 判断VS编译器版本
#ifdef DEVELOPMENT_PLATFORM_VS
#pragma chMSG(编译器版本: VS)

//#include <Intrin.h> //_ReturnAddress()
//#include <strsafe.h>
//#include <Winternl.h>

#else //!(_MSC_VER >= 1300) vc6
#pragma chMSG(编译器版本: VC6.0)

#include <afxmt.h> //CCriticalSection

#define sscanf_s		sscanf
#ifdef UNICODE
#define _stscanf_s		wscanf
#else
#define _stscanf_s		sscanf
#endif
#define _vstprintf_s	_vsnprintf
#define _stprintf_s		sprintf
#define _sntprintf_s	_snprintf
#define _tcsncpy_s		_tcsncpy
#define _tcslwr_s		_tcslwr
#define swprintf_s		swprintf
#define _tcscpy_s		_tcscpy
#define _tcscat_s		_tcscat

#endif


#ifdef UNICODE
#pragma chMSG(使用 Unicode 字符集)
#define TString			std::wstring
#define TStringStream	std::wstringstream
#else //!UNICODE
#pragma chMSG(使用多字节字符集)
#define TString			std::string
#define TStringStream	std::stringstream
#endif //UNICODE


#ifdef DEBUG
#pragma chMSG(版本: Debug)
#else //!DEBUG
#pragma chMSG(版本: Release)
#endif //DEBUG

#include <specstrings.h>    // for _In_, etc.

#ifndef __in
#define		__in
#endif
#ifndef __out
#define		__out
#endif
#ifndef __in_opt
#define		__in_opt
#endif
#ifndef _In_opt_
#define		_In_opt_
#endif
#ifndef __out_opt
#define		__out_opt
#endif
#ifndef __inout
#define		__inout
#endif
#ifndef __inout_opt
#define		__inout_opt
#endif
#ifndef _Inout_
#define		_Inout_
#endif
#ifndef _In_
#define		_In_
#endif	
#ifndef _Out_
#define		_Out_
#endif
#ifndef _Inout_opt_
#define		_Inout_opt_
#endif
#ifndef _Out_opt_
#define		_Out_opt_
#endif
#ifndef _Reserved_
#define		_Reserved_
#endif

//////////////////////////////////////////////////////////////////////////
// Outputdebugstring 异常码
#ifndef DBG_PRINTEXCEPTION_C
#define DBG_PRINTEXCEPTION_C	((DWORD   )0x40010006L)
#endif

//////////////////////////////////////////////////////////////////////////
// 定义了一些系统没导出的结构
//#include "ntstructure.h"
//#include "ntdll.pdb_1751003260CA42598C0FB326585000ED.h"

typedef LONG NTSTATUS;

#ifndef _WIN64
#ifdef DEVELOPMENT_PLATFORM_VS 
#ifdef WOW64_INCLUDE
#pragma chMSG(WOW64ext include)
//#include "Wow64/w64wow64.h"
//#include "Wow64/w64wow64defs.h"
//#include "Wow64/internal.h"
//#include "Wow64/windef.h"

#include "rewolf-wow64ext-master/include/wow64ext.h"
#include "rewolf-wow64ext-master/include/internal.h"

extern DWORD64 g_hNtDll64;
extern DWORD64 g_hKernel3264;
extern DWORD64 g_hUser3264;

extern BOOL g_isWow64;

#endif
//#include "boost/regex.hpp"
#endif
#endif

//////////////////////////////////////////////////////////////////////////
// LuaLib
#ifdef LUALIB_INCLUDE
#define STACK_TOP     -1
#define STACK_BOTTOM   1
#include "luaplus/LuaVirtualMachine.h"
#include "luaplus/luascript.h"
#include "luaplus/luadebugger.h"
#include "luaplus/lualib/luainc.h"
#endif

//////////////////////////////////////////////////////////////////////////
// libcurl
#ifdef LIBCURL_INCLUDE
#define USE_OPENSSL
#define CURL_STATICLIB
#include <curl/curl.h>
#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "wldap32.lib")

#ifdef _WIN64

#ifdef _DEBUG
#pragma comment(lib, "libcurl64d.lib")
#else
#pragma comment(lib, "libcurl64.lib")
#endif

#else

#ifdef _DEBUG
#pragma comment(lib, "libcurld.lib")
#else
#pragma comment(lib, "libcurl.lib")
#endif

#endif

#if _MSC_VER > 1200 //vc6+

#ifdef _WIN64

#ifdef _DEBUG
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\debug\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\debug\\lib\\ssleay32.lib")
#else
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\release\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\release\\lib\\ssleay32.lib")
#endif

#else

#ifdef _DEBUG
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\debug\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\debug\\lib\\ssleay32.lib")
#else
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\release\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\release\\lib\\ssleay32.lib")
#endif

#endif //WIN64

#else //vc6

#ifdef _DEBUG
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#else
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#endif

#endif

#endif

//////////////////////////////////////////////////////////////////////////
// 纯函数体
#define NAKED __declspec(naked)

//////////////////////////////////////////////////////////////////////////
// 定义结构体
#define STRUCT(name) typedef struct _tag_##name name; struct _tag_##name

#pragma pack(push)
#pragma pack(16)
//////////////////////////////////////////////////////////////////////////
// 为方便读写XMM，YMM寄存器的数据
// XMM是16字节，YMM是32字节
typedef union _XMMDATA {
	struct {
		CHAR data[32]; 
	} _byte;

	struct {
		SHORT data[16];
	} _short;

	struct {
		LONG data[8];
	} _long;

	struct {
		LONGLONG data[4];
	} _longlong;

	struct {
		float data[8];
	} _float;

	struct {
		double data[4];
	} _double;

	_XMMDATA() {
		ZeroMemory(this, sizeof(_XMMDATA));
	}
} XMMDATA;
#pragma pack(pop)

//////////////////////////////////////////////////////////////////////////
// 保存xmm0~xmm6
#define BACKUPXMM06 \
	XMMDATA x0,x1,x2,x3,x4,x5,x6,x7/*,x8,x9,x10,x11,x12,x13,x14,x15*/; \
	__asm movups xmmword ptr ds:[x0], xmm0 \
	__asm movups xmmword ptr ds:[x1], xmm1 \
	__asm movups xmmword ptr ds:[x2], xmm2 \
	__asm movups xmmword ptr ds:[x3], xmm3 \
	__asm movups xmmword ptr ds:[x4], xmm4 \
	__asm movups xmmword ptr ds:[x5], xmm5 \
	__asm movups xmmword ptr ds:[x6], xmm6 \
	__asm movups xmmword ptr ds:[x7], xmm7 \
	/*__asm movups xmmword ptr ds:[x8], xmm8*/ \
	/*__asm movups xmmword ptr ds:[x9], xmm9*/ \
	/*__asm movups xmmword ptr ds:[x10], xmm10*/ \
	/*__asm movups xmmword ptr ds:[x11], xmm11*/ \
	/*__asm movups xmmword ptr ds:[x12], xmm12*/ \
	/*__asm movups xmmword ptr ds:[x13], xmm13*/ \
	/*__asm movups xmmword ptr ds:[x14], xmm14*/ \
	/*__asm movups xmmword ptr ds:[x15], xmm15*/


//////////////////////////////////////////////////////////////////////////
// 恢复xmm0~6
#define RECOVERXMM06 \
	__asm movups xmm0, xmmword ptr ds:[x0] \
	__asm movups xmm1, xmmword ptr ds:[x1] \
	__asm movups xmm2, xmmword ptr ds:[x2] \
	__asm movups xmm3, xmmword ptr ds:[x3] \
	__asm movups xmm4, xmmword ptr ds:[x4] \
	__asm movups xmm5, xmmword ptr ds:[x5] \
	__asm movups xmm6, xmmword ptr ds:[x6] \
	__asm movups xmm7, xmmword ptr ds:[x7] \
	/*__asm movups xmm8, xmmword ptr ds:[x8]*/ \
	/*__asm movups xmm9, xmmword ptr ds:[x9]*/ \
	/*__asm movups xmm10, xmmword ptr ds:[x10]*/ \
	/*__asm movups xmm11, xmmword ptr ds:[x11]*/ \
	/*__asm movups xmm12, xmmword ptr ds:[x12]*/ \
	/*__asm movups xmm13, xmmword ptr ds:[x13]*/ \
	/*__asm movups xmm14, xmmword ptr ds:[x14]*/ \
	/*__asm movups xmm15, xmmword ptr ds:[x15]*/ 

// 释放指针宏
#define RELESE(P) if (P)		\
{								\
	delete P;					\
	P = NULL;					\
}

// 释放数组宏
#define RELESE_ARRAY(P) if (P)  \
{								\
	delete[] P;					\
	P = NULL;					\
}

inline TString thisFileName(LPCTSTR strFilePath) {
	TString f = strFilePath;
	TString f2 = f.substr(f.find_last_of(TEXT("\\"))+1, f.size());
	return f2;
}

//////////////////////////////////////////////////////////////////////////
// 取错误信息
#define FAIL_MSG(msg) { \
	TCHAR buff[KB] = {0}; \
	Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d):%s failed. %s"), thisFileName(TFILE).c_str(), __LINE__, msg, Output::GetFormattedMessageString().c_str()); \
}

//////////////////////////////////////////////////////////////////////////
// catch all
//#ifdef UNICODE
//#define catch_all() \
//	catch (CMemoryException* e) \
//	{ \
//		TCHAR szError[MAX_PATH+1] = {0}; \
//		e->GetErrorMessage(szError, MAX_PATH); \
//		Output::DbgPrint(TEXT("%S(%d) [Thread:%u]catch CMemoryException:%s"), __FILE__, __LINE__, GetCurrentThreadId(), szError); \
//	} \
//	catch (CFileException* e) \
//	{ \
//		TCHAR szError[MAX_PATH+1] = {0}; \
//		e->GetErrorMessage(szError, MAX_PATH); \
//		Output::DbgPrint(TEXT("%S(%d) [Thread:%u]catch CFileException:%s"), __FILE__, __LINE__, GetCurrentThreadId(), szError); \
//	} \
//	catch (CException* e) \
//	{ \
//		TCHAR szError[MAX_PATH+1] = {0}; \
//		e->GetErrorMessage(szError, MAX_PATH); \
//		Output::DbgPrint(TEXT("%S(%d) [Thread:%u]catch CException:%s"), __FILE__, __LINE__, GetCurrentThreadId(), szError); \
//	} \
//	catch (...) \
//	{ \
//		Output::DbgPrint(TEXT("%S(%d) [Thread:%u]catch ...:%s"), __FILE__, __LINE__, GetCurrentThreadId(), GetFormattedMessageString().c_str()); \
//	}
//#else
#define catch_all() \
	catch (CMemoryException* e) \
	{ \
		TCHAR szError[MAX_PATH+1] = {0}; \
		e->GetErrorMessage(szError, MAX_PATH); \
		TCHAR buff[KB] = {0}; \
		Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d) [Thread:%u] CMemoryException:%s"), thisFileName(TFILE).c_str(), __LINE__, GetCurrentThreadId(), szError); \
	} \
	catch (CFileException* e) \
	{ \
		TCHAR szError[MAX_PATH+1] = {0}; \
		e->GetErrorMessage(szError, MAX_PATH); \
		TCHAR buff[KB] = {0}; \
		Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d) [Thread:%u] CFileException:%s"), thisFileName(TFILE).c_str(), __LINE__, GetCurrentThreadId(), szError); \
	} \
	catch (CException* e) \
	{ \
		TCHAR szError[MAX_PATH+1] = {0}; \
		e->GetErrorMessage(szError, MAX_PATH); \
		TCHAR buff[KB] = {0}; \
		Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d) [Thread:%u] CException:%s"), thisFileName(TFILE).c_str(), __LINE__, GetCurrentThreadId(), szError); \
	} \
	catch (...) \
	{ \
		TCHAR buff[KB] = {0}; \
		Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d) [Thread:%u] catch...:%s"), thisFileName(TFILE).c_str(), __LINE__, GetCurrentThreadId(), Output::GetFormattedMessageString().c_str()); \
	}
//#endif

#define catchSeException() \
	catch(CSeException *e) { \
		TCHAR szError[KB]; \
		e->GetErrorMessage(szError,KB); \
		TCHAR buff[KB] = {0}; \
		Output::DbgPrintBuffer(buff, KB, TEXT("%s(%d) [Thread:%u] SeException:%s"), thisFileName(TFILE).c_str(), __LINE__, GetCurrentThreadId(), szError); \
		if (e->m_bLogFile) \
		{ \
			TCHAR sModName[MAX_PATH]; \
			HMODULE hMod; \
			Common::GetModuleNameByAddress(e->GetExceptionAddress(), sModName); \
			Common::GetModuleBaseByAddress(e->GetExceptionAddress(), &hMod); \
			e->LogFile(thisFileName(TFILE).c_str(), __LINE__, szError, sModName, hMod); \
		} \
		e->Delete(); \
	}

void HandleExcept();
#ifdef STACKTRACER_HEADER
#define exceptStackTracer() \
	__except(StackTracer::ExceptionFilter(GetExceptionInformation())) \
	{ \
		HandleExcept(); \
	}
#endif

//////////////////////////////////////////////////////////////////////////
// SendMessage宏，避免目标窗口无响应时发送消息会卡死
// Output::DbgPrintA("sndmsg %p,%d,%d,%p", h, msg,wp,lp);
#define SENDMSG(h, msg, wp, lp) { \
	if (::IsHungAppWindow(h)) { \
	Output::DbgPrintA("S:%p,窗口无响应", h); \
	} \
	else { \
	::SendMessage(h, msg, wp, lp); } \
}

#define SENDMSG2(h, msg, wp, lp) { \
	if (0 && ::IsHungAppWindow(h)) { \
	Output::DbgPrintA("S:%p,窗口无响应", h); \
	} \
	else { \
	::SendMessage(h, msg, wp, lp); } \
}

#define POSTMSG(h, msg, wp, lp) { \
	if (::IsHungAppWindow(h)) { \
	Output::DbgPrintA("P:%p,窗口无响应", h); \
	} \
	else { \
	::PostMessage(h, msg, wp, lp); } \
}
//////////////////////////////////////////////////////////////////////////
// vc6 _ReturnAddress()
// 判断VS编译器版本
#ifndef DEVELOPMENT_PLATFORM_VS
static NAKED VOID* _ReturnAddress() {
	__asm {
		mov eax, [ebp+4]
		ret
	}
}
#endif
//////////////////////////////////////////////////////////////////////////
// caller
#define _caller() *(void**)_AddressOfReturnAddress()
#define CALLER VOID *cl = _caller()

#ifdef _WIN64
//////////////////////////////////////////////////////////////////////////
// x64 pushad popad
#define PUSHADSIZE 0x78

#define pushad_x64 \
	/*__asm push rax*/	/*+08*/\
	__asm push rbx	/*+10*/\
	__asm push rcx	/*+18*/\
	__asm push rdx	/*+20*/\
	__asm push rbp	/*+28*/\
	__asm push rsi	/*+30*/\
	__asm push rdi	/*+38*/\
	__asm push r8	/*+40*/\
	__asm push r9	/*+48*/\
	__asm push r10	/*+50*/\
	__asm push r11	/*+58*/\
	__asm push r12	/*+60*/\
	__asm push r13	/*+68*/\
	__asm push r14	/*+70*/\
	__asm push r15	/*+78*/\
	__asm pushfq	/*+80*/

#define popad_x64 \
	__asm popfq		/*rsp+0*/\
	__asm pop r15	/*rsp+1*/\
	__asm pop r14	/*rsp+2*/\
	__asm pop r13	/*rsp+3*/\
	__asm pop r12	/*rsp+4*/\
	__asm pop r11	/*rsp+5*/\
	__asm pop r10	/*rsp+6*/\
	__asm pop r9	/*rsp+7*/\
	__asm pop r8	/*rsp+8*/\
	__asm pop rdi	/*rsp+9*/\
	__asm pop rsi	/*rsp+10*/\
	__asm pop rbp	/*rsp+11*/\
	__asm pop rdx	/*rsp+12*/\
	__asm pop rcx	/*rsp+13*/\
	__asm pop rbx	/*rsp+14*/\
	/*__asm pop rax*/	/*rsp+15*/

// x64 push rcx,rdx,r8,r9
#define X64_Push4 \
	/*sub, rsp, 0x80*/\
	__asm push rcx\
	__asm push rdx\
	__asm push r8\
	__asm push r9\
	/*__asm push r10*/\
	/*__asm push r11*/\
	/*__asm push r12*/

#define X64_Pop4 \
	/*__asm pop r12*/\
	/*__asm pop r11*/\
	/*__asm pop r10*/\
	__asm pop r9\
	__asm pop r8\
	__asm pop rdx\
	__asm pop rcx\
	/*add rsp,0x80*/

#endif //_WIN64


namespace Output
{
#define DbgStrBufferLengthA	65534
#define DbgStrBufferLengthW	32767
#define BUFFERSIZE 1024

	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，MaxBufferLength：100k bytes
	// 如果是没参数的字符串，最好使用OutputDebugString，有些不合适的%导致异常
	// 如下：
	// DbgPrint(TEXT("// % 25")); //error
	// OutputDebugString(TEXT("// % 25")); //ok
	HRESULT WINAPIV DbgPrintA(LPCSTR lpszFmt, ...);
	HRESULT WINAPIV DbgPrintW(LPCWSTR lpszFmt, ...);
#ifdef UNICODE
#define DbgPrint DbgPrintW
#else
#define DbgPrint DbgPrintA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，需提供buff,len参数
	HRESULT WINAPIV DbgPrintBufferA(LPSTR buff, size_t len, LPCSTR lpszFmt, ...);
	HRESULT WINAPIV DbgPrintBufferW(LPWSTR buff, size_t len, LPCWSTR lpszFmt, ...);
#ifdef UNICODE
#define DbgPrintBuffer DbgPrintBufferW
#else
#define DbgPrintBuffer DbgPrintBufferA
#endif


	//////////////////////////////////////////////////////////////////////////
	// 根据当前的错误号，获取其错误字符串
	LPTSTR GetFormattedMessage(LPTSTR szText);
	TString GetFormattedMessageString();
	TString GetFormattedMessageStringByErrorNo(DWORD nError);
	
	//////////////////////////////////////////////////////////////////////////
	// 把数值转换为千位分隔符的格式字符串，借用java的名称
	// 如输入123456，输出"123,456"
	std::string toLocaleStringA(LONGLONG val);
	std::wstring toLocaleStringW(LONGLONG val);
#ifdef UNICODE
#define toLocaleString toLocaleStringW
#else
#define toLocaleString toLocaleStringA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 把数值转换为中文钱币的格式字符串
	// 如输入123456，输出"12万3456"
	std::string toRMBA(LONGLONG val, bool padding=true, bool big5=false);
	std::wstring toRMBW(LONGLONG val, bool padding=true, bool big5=false);
#ifdef UNICODE
#define toRMB toRMBW
#else
#define toRMB toRMBA
#endif

#define _UPPERCASE	0x1
#define _LOWERCASE	0x2
#define _NUMBER		0x4
	//////////////////////////////////////////////////////////////////////////
	// 随机大小写数字组合的字符串
	std::string RandomStrA(size_t nCount, int RType=_UPPERCASE|_LOWERCASE|_NUMBER);
	std::wstring RandomStrW(size_t nCount, int RType=_UPPERCASE|_LOWERCASE|_NUMBER);
#ifdef UNICODE
#define RandomStr RandomStrW
#else
#define RandomStr RandomStrA
#endif
}

namespace Common
{
#define BUFSIZE 4096

	// 浮点千
#define _double_Thousand			(DOUBLE)(1000.0f)
	// 浮點万
#define _double_Ten_Thousand		(DOUBLE)(10*_double_Thousand)
	// 浮點百万
#define _double_Million				(DOUBLE)(1000*_double_Thousand)
	// 浮點億
#define _double_Hundred_Million		(DOUBLE)(100*_double_Million)
	// 浮點十億
#define _double_Billion				(DOUBLE)(1000*_double_Million)

	//////////////////////////////////////////////////////////////////////////
	// kb, mb, gb
#define KB				1024
#define MB				1024*KB
#define GB				1024*MB

	//////////////////////////////////////////////////////////////////////////
	// 页的大小，4k
#define PAGE_SIZE		4*KB

	//////////////////////////////////////////////////////////////////////////
	// 转16进制数
#define Hex(a) 0x##a

	//////////////////////////////////////////////////////////////////////////
	// 在nt层hook用到
#define CallOnce_Start(X) \
	static DWORD tls_##X = TLS_OUT_OF_INDEXES; \
	if(tls_##X == TLS_OUT_OF_INDEXES) \
		tls_##X = TlsAlloc(); \
	if(TlsGetValue(tls_##X)) \
		return STATUS_SUCCESS; \
	else { TlsSetValue(tls_##X, (LPVOID)1);

#define CallOnce_End(X) \
	TlsSetValue(tls_##X, (LPVOID)0); }

	//////////////////////////////////////////////////////////////////////////
	// OutputDebugString使用到的filemapping
#define DBWIN_BUFFER_XP   L"\\BaseNamedObjects\\DBWIN_BUFFER"
#define DBWIN_BUFFER_WIN7 L"\\Sessions\\1\\BaseNamedObjects\\DBWIN_BUFFER"

	//////////////////////////////////////////////////////////////////////////
	// n,d只能是数字常量，不能使用变量
//#define SETBYTE(n,d) #n"_"#d

#define SETBYTE(n,d) n,(UCHAR)d

	//in: 2,3; out:TEXT("2_3")
	LPCTSTR SetByte(INT n, INT d);

	// 初始化数组，不作越界检测
	//InitData(data, SETBYTE(6,4),SETBYTE(8,9));
	//data:0404040404040909090909090909
	VOID InitData(UCHAR* data, ...);

	//////////////////////////////////////////////////////////////////////////
	// 类似系统的MAKEWORD,MAKELONG
#define HEX_WORD(a, b)			MAKEWORD(a, b) //0x##b##a
#define HEX_DWORD(a, b, c, d)	MAKELONG(MAKEWORD(a,b), MAKEWORD(c,d)) //0x##d##c##b##a

	// 从字符串中转换为UCHAR值
	// in:	"1a"
	// out:	0x1a
	UCHAR uCharFromStringA(CHAR* s);
	UCHAR uCharFromStringW(WCHAR* s);
#ifdef UNICODE
#define uCharFromString uCharFromStringW
#else
#define uCharFromString uCharFromStringA
#endif

	// 从字符串中转换为SHORT值
	// in:	"1a 2b"
	// out:	0x2b1a
	USHORT uShortFromStringA(CHAR* s, bool bSpace=true);
	USHORT uShortFromStringW(WCHAR* s, bool bSpace=true);
#ifdef UNICODE
#define uShortFromString uShortFromStringW
#else
#define uShortFromString uShortFromStringA
#endif

	// 从字符串中转换为LONG值
	// in:	"1a 2b d0 e1"
	// out:	0xe1d02b1a
	ULONG uLongFromStringA(CHAR* s, bool bSpace=true);
	ULONG uLongFromStringW(WCHAR* s, bool bSpace=true);
#ifdef UNICODE
#define uLongFromString uLongFromStringW
#else
#define uLongFromString uLongFromStringA
#endif
	
	// 从字符串中转换为LONGLONG值
	// in:	"1a 2b d0 e1"
	// out:	0xe1d02b1a
	ULONGLONG uLongLongFromStringA(CHAR* s, bool bSpace=true);
	ULONGLONG uLongLongFromStringW(WCHAR* s, bool bSpace=true);
#ifdef UNICODE
#define uLongLongFromString uLongLongFromStringW
#else
#define uLongLongFromString uLongLongFromStringA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 模块的基址。如果隐式加载会导致取模块GetModuleFileName失败
	EXTERN_C IMAGE_DOS_HEADER __ImageBase;

	//////////////////////////////////////////////////////////////////////////
	// 获取当前exe的运行路径，确保path长度不少于MAX_PATH
	__inline CHAR* CurrentDirA(CHAR* path)
	{
		CHAR buff[MAX_PATH] = {0};
		::GetCurrentDirectoryA(MAX_PATH-1, buff);
		StringCchCopyA(path, MAX_PATH, buff);
		return path;
	}
	// 获取当前exe的运行路径，确保path长度不少于MAX_PATH
	__inline WCHAR* CurrentDirW(WCHAR* path)
	{
		WCHAR buff[MAX_PATH] = {0};
		::GetCurrentDirectoryW(MAX_PATH-1, buff);
		StringCchCopyW(path, MAX_PATH, buff);
		return path;
	}
#ifdef UNICODE
#define CurrentDir CurrentDirW
#else
#define CurrentDir CurrentDirA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 取模块的完整路径名，确保path长度不少于MAX_PATH
	__inline CHAR* CurrentModulePathA(LPCSTR lpModuleName, CHAR* path)
	{
		UNREFERENCED_PARAMETER(lpModuleName);
		CHAR buff[MAX_PATH] = {0};
		::GetModuleFileNameA(reinterpret_cast<HMODULE>(&Common::__ImageBase),
			/*::GetModuleHandleA(lpModuleName),*/
			buff,
			MAX_PATH-1);
		StringCchCopyA(path, MAX_PATH, buff);
		return path;
	}
	// 取模块的完整路径名，确保path长度不少于MAX_PATH
	__inline WCHAR* CurrentModulePathW(LPCWSTR lpModuleName, WCHAR* path)
	{
		UNREFERENCED_PARAMETER(lpModuleName);
		WCHAR buff[MAX_PATH] = {0};
		::GetModuleFileNameW(reinterpret_cast<HMODULE>(&Common::__ImageBase),
			/*::GetModuleHandleW(lpModuleName),*/
			buff,
			MAX_PATH-1);
		StringCchCopyW(path, MAX_PATH, buff);
		return path;
	}
#ifdef UNICODE
#define CurrentModulePath CurrentModulePathW
#else
#define CurrentModulePath CurrentModulePathA
#endif
	
	//////////////////////////////////////////////////////////////////////////
	// 设为当前目录的指定文件名，确保path长度不少于MAX_PATH
	__inline CHAR* CurrentPathFileA(CHAR* path, CHAR* file)
	{
		CHAR buff[MAX_PATH] = {0};
		CurrentModulePathA(file, buff);
		CHAR* p1 = strrchr(buff, '\\');
		if (p1)
		{
			StringCchPrintfA(p1+1, 10, "");
		}
		//StringCchCatA(buff, MAX_PATH-lstrlenA(buff), "\\");
		StringCchCatA(buff, MAX_PATH-lstrlenA(buff), file);
		StringCchCopyA(path, MAX_PATH, buff);
		return path;
	}
	// 设为当前目录的指定文件名，确保path长度不少于MAX_PATH
	__inline WCHAR* CurrentPathFileW(WCHAR* path, WCHAR* file)
	{
		WCHAR buff[MAX_PATH] = {0};
		CurrentModulePathW(file, buff);
		WCHAR* p1 = wcsrchr(buff, L'\\');
		if (p1)
		{
			StringCchPrintfW(p1+1, 10, L"");
		}
		//StringCchCatW(buff, MAX_PATH-lstrlenW(buff), L"\\");
		StringCchCatW(buff, MAX_PATH-lstrlenW(buff), file);
		StringCchCopyW(path, MAX_PATH, buff);
		return path;
	}
#ifdef UNICODE
#define CurrentPathFile CurrentPathFileW
#else
#define CurrentPathFile CurrentPathFileA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 取系统支持字体集，结果保存在预定义的vector变量:g_vecSystemFont
	// 由于使用了枚举的机制，调用完后要间隔一定时间取g_vecSystemFont，但size也是0？
	VOID GetSystemFont();

	STRUCT(SYSTEMFONT)
	{
		LOGFONT LogFont;
		TEXTMETRIC TextMetric;
	};
	static vector<SYSTEMFONT>g_vecSystemFont;


#define GET_MODULE_HANDLE_EX_FLAG_PIN                 (0x00000001)
#define GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT  (0x00000002)
#define GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS        (0x00000004)

	//////////////////////////////////////////////////////////////////////////
	// 由地址值获取当前进程的模块名，请定义buff长度为MAX_PATH
	__inline LPTSTR GetModuleNameByAddress(LPVOID Addr, LPTSTR buff)
	{
		ZeroMemory(buff, MAX_PATH);
		::GetMappedFileName(::GetCurrentProcess(), Addr, buff, MAX_PATH-1);
		return buff;
	}

#ifdef DEVELOPMENT_PLATFORM_VS
	//////////////////////////////////////////////////////////////////////////
	// 由地址值获取当前进程的模块的基址
	__inline VOID GetModuleBaseByAddress(LPVOID Addr, HMODULE* pMod)
	{
		// error C2039: “GetModuleHandleEx”: 不是“`global namespace'”的成员
		// WINVER,_WIN32_WINNT,设0x501; _WIN32_IE设0x600
		::GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, reinterpret_cast<LPCTSTR>(Addr), pMod);
	}
#endif


	//////////////////////////////////////////////////////////////////////////
	// 通用的功能：字节流vector存放到string：s
	__inline VOID StreamVectorToString(const vector<UCHAR>& v, string& s)
	{
		s.resize(v.size());

#ifdef DEVELOPMENT_PLATFORM_VS
		s.assign(v.begin(), v.end());
#else
		CopyMemory((UCHAR*)s.data(), v.begin(), v.size());
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// 通用的功能：字节流u存放到string：s，长度n
	__inline VOID StreamToString(const UCHAR* u, size_t n, string& s)
	{
		const UCHAR* tu = u;
		if (::IsBadReadPtr(u, 4))
		{
			tu = (const UCHAR*)"\xba\xad\xf0\x0d";
			n = 4;
		}
		vector<UCHAR> v;
		v.clear();
		for (size_t i=0; i<n; i++)
		{
			v.push_back(tu[i]);
		}
		StreamVectorToString(v, s);
	}

	__inline VOID StreamToString64(__int64 u, size_t n, string& s)
	{
		__int64* tu = &u;
		if (::IsBadReadPtr(tu, 8))
		{
			tu = (__int64*)"\xba\xad\xf0\x0d";
			n = 4;
		}
		vector<UCHAR> v;
		v.clear();
		for (size_t i=0; i<n; i++)
		{
			v.push_back((UCHAR)*(__int64*)(u+i));
		}
		StreamVectorToString(v, s);
	}

	//////////////////////////////////////////////////////////////////////////
	// 通用的功能：字节流转换为字符串格式
	__inline VOID StreamToString(const string& src, string& dst, bool uppercase)
	{
		stringstream sstr;
		sstr.setf(std::ios_base::hex, std::ios::basefield);
		if(uppercase)
			sstr.setf(std::ios_base::uppercase);

		sstr.fill('0');

		for (size_t i=0; i<src.length(); i++)
		{
			sstr << std::setw(2) << (UINT)(UCHAR)(src[i]);
		}

		dst = sstr.str();
	}

	//////////////////////////////////////////////////////////////////////////
	// 通用的功能：字符串转换为vector<UCHAR>
	__inline VOID StringToStreamVector(const string& s, vector<UCHAR>& v)
	{
		stringstream sstr;
		sstr.setf(std::ios_base::hex, std::ios::basefield);

		v.clear();
		for (size_t i=0; i<s.length(); i+=2)
		{
			string strtmp(s, i, 2);
			sstr.clear();
			sstr << strtmp;
			int ntmp;
			sstr >> ntmp;
			v.push_back((UCHAR)ntmp);
		}
	}

	// 判断文件是否存在
	__inline BOOL IsFileExistA(const string& csFile)
	{
		DWORD dwAttrib = GetFileAttributesA(csFile.c_str());
		return (DWORD)INVALID_HANDLE_VALUE != dwAttrib && 0 == (dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
	}
	__inline BOOL IsFileExistW(const wstring& csFile)
	{
		DWORD dwAttrib = GetFileAttributesW(csFile.c_str());
		return (DWORD)INVALID_HANDLE_VALUE != dwAttrib && 0 == (dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
	}
#ifdef UNICODE
#define IsFileExist IsFileExistW
#else
#define IsFileExist IsFileExistA
#endif

	// 判断文件夹是否存在
	__inline BOOL IsDirExistA(const string & csDir)
	{
		DWORD dwAttrib = GetFileAttributesA(csDir.c_str());
		return (DWORD)INVALID_HANDLE_VALUE != dwAttrib && 0 != (dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
	}
	__inline BOOL IsDirExistW(const wstring & csDir)
	{
		DWORD dwAttrib = GetFileAttributesW(csDir.c_str());
		return (DWORD)INVALID_HANDLE_VALUE != dwAttrib && 0 != (dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
	}
#ifdef UNICODE
#define IsDirExist IsDirExistW
#else
#define IsDirExist IsDirExistA
#endif

	//// 判断文件或文件夹是否存在
	//__inline BOOL IsPathExist(const TString & csPath)
	//{
	//	DWORD dwAttrib = GetFileAttributes(csPath.c_str());
	//	return (DWORD)INVALID_HANDLE_VALUE != dwAttrib;
	//}

	// 变种(听说会更快一点)
	__inline BOOL IsPathExistA(const string & csPath)
	{
		WIN32_FILE_ATTRIBUTE_DATA attrs = { 0 };
		return 0 != GetFileAttributesExA(csPath.c_str(), GetFileExInfoStandard, &attrs);
	}
	__inline BOOL IsPathExistW(const wstring & csPath)
	{
		WIN32_FILE_ATTRIBUTE_DATA attrs = { 0 };
		return 0 != GetFileAttributesExW(csPath.c_str(), GetFileExInfoStandard, &attrs);
	}
#ifdef UNICODE
#define IsPathExist IsPathExistW
#else
#define IsPathExist IsPathExistA
#endif


	//////////////////////////////////////////////////////////////////////////
	// begin_ntndis
	//
	// Counted String
	//

	typedef USHORT RTL_STRING_LENGTH_TYPE;



	//////////////////////////////////////////////////////////////////////////
#if 0
	// 编译错误
#include <ntstatus.h>
#endif
#ifndef NT_SUCCESS
#define NT_SUCCESS(Status) ((NTSTATUS)(Status)>=0)
#endif

#ifndef STATUS_SUCCESS
#define STATUS_SUCCESS					0x00000000L
#endif

#ifndef STATUS_UNSUCCESSFUL
#define STATUS_UNSUCCESSFUL				0xC0000001L
#endif

#ifndef STATUS_NOT_IMPLEMENTED
#define STATUS_NOT_IMPLEMENTED			0xC0000002L
#endif

#ifndef STATUS_INFO_LENGTH_MISMATCH
#define STATUS_INFO_LENGTH_MISMATCH		0xC0000004L
#endif

#ifndef STATUS_INVALID_PARAMETER
#define STATUS_INVALID_PARAMETER		0xC000000DL
#endif

#ifndef STATUS_ACCESS_DENIED
#define STATUS_ACCESS_DENIED			0xC0000022L
#endif

#ifndef STATUS_BUFFER_TOO_SMALL
#define STATUS_BUFFER_TOO_SMALL			0xC0000023L
#endif

#ifndef STATUS_OBJECT_NAME_NOT_FOUND
#define STATUS_OBJECT_NAME_NOT_FOUND	0xC0000034L
#endif

#ifndef STATUS_INVALID_ADDRESS
#define STATUS_INVALID_ADDRESS			0xC0000141L
#endif

}

namespace Undocument
{

//#ifndef DEVELOPMENT_PLATFORM_VS
//	// SwitchToThisWindow
//	typedef VOID (WINAPI* fnSwitchToThisWindow)(
//		_In_ HWND hWnd,
//		_In_ BOOL fAltTab
//		);
//	static fnSwitchToThisWindow SwitchToThisWindow = 
//		(fnSwitchToThisWindow)::GetProcAddress(hUser32, "SwitchToThisWindow");
//
//	// GetProcessId
//	typedef DWORD (WINAPI* fnGetProcessId)(
//		__in HANDLE Process
//		);
//	static fnGetProcessId GetProcessId = 
//		(fnGetProcessId)::GetProcAddress(hKernel32, "GetProcessId");
//
//#endif
	
	// 取系统版本信息
	static OSVERSIONINFO g_OsVerInfo = {sizeof(OSVERSIONINFO)};
	//static UNICODE_STRING g_usDbWinBuffer = {0};

	//typedef enum _WIN_PLATFORM {
	//	WINXP,
	//	WIN7,
	//	WIN10
	//} WIN_PLATFORM;
	//static WIN_PLATFORM g_WinPlatForm = WINXP;

#ifdef __AFXWIN_H__ // MFC requires
	// 需要前端操作的鼠标，键盘的输入
	static CCriticalSection g_cs_mouse_kd_input;
	// 保存要操作的目标窗口
	static HWND g_hWndMouseKd = NULL;
#endif

	// 自定义一个类，用来初始化一些函数和变量
	class MyGlobalInitial
	{
	public:
		MyGlobalInitial()
		{
			ZeroMemory(fmtFileA, sizeof(fmtFileA));
			ZeroMemory(fmtFileW, sizeof(fmtFileW));
			ZeroMemory(fmtNtFileA, sizeof(fmtNtFileA));

#ifdef DEVELOPMENT_PLATFORM_VS
			CALLERNAME();
#endif
			TCHAR buff[MAX_PATH] = {0};
			Output::DbgPrintBuffer(buff, MAX_PATH, TEXT("line (%d)"), __LINE__);
			//#ifdef UNICODE
			//Output::DbgPrint(TEXT("%S(%d)"), __FILE__, __LINE__);
			//#else
			//Output::DbgPrint(TEXT("%s(%d)"), __FILE__, __LINE__);
			//#endif

			::GetVersionEx(&g_OsVerInfo);
			Output::DbgPrintBuffer(buff, MAX_PATH, TEXT("系统版本：%d.%d"), g_OsVerInfo.dwMajorVersion, g_OsVerInfo.dwMinorVersion);


#ifndef _WIN64
#ifdef DEVELOPMENT_PLATFORM_VS
#ifdef WOW64_INCLUDE
			//InitializeW64oWoW64();
			if (InitialX64())
			{
				Output::DbgPrintBuffer(buff, MAX_PATH, TEXT("InitialX64() %I64p,%I64p,%I64p"), g_hNtDll64, g_hKernel3264, g_hUser3264);
			}
			else
			{
				if (!Common::Is64BitOS())
					Output::DbgPrintBuffer(buff, MAX_PATH, TEXT("InitialX64() failed: 当前是32位系统"));
				else
					FAIL_MSG(TEXT("InitialX64()"));
			}
#endif
#endif
#endif

#ifdef DEBUGSTRING_TO_FILE
			CHAR path[MAX_PATH] = {"out.txt"};
			//Common::CurrentPathFileA(path, "out.txt");

			// 保存cout流缓冲区指针
			coutBuf = std::cout.rdbuf();

			of.open(path, ios_base::out);
			// 获取文件out.txt流缓冲区指针
			std::streambuf* fileBuf = of.rdbuf();
			// 设置cout流缓冲区指针为out.txt的流缓冲区指针
			std::cout.rdbuf(fileBuf);
#endif
		}
		~MyGlobalInitial()
		{
#ifdef DEBUGSTRING_TO_FILE
			of.flush();
			of.close();

			// 恢复cout原来的流缓冲区指针
			std::cout.rdbuf(coutBuf);
#endif
		}

		CHAR fmtFileA[DbgStrBufferLengthA];
		WCHAR fmtFileW[DbgStrBufferLengthW];
		CHAR fmtNtFileA[DbgStrBufferLengthA];

		std::streambuf* coutBuf;
		std::ofstream of;
	};

	static MyGlobalInitial g_Global; //会导致重复的加载？？？

	//////////////////////////////////////////////////////////////////////////
	// 校验当前的系统版本
	inline BOOL VerifyVersion(DWORD major, DWORD minor, WORD sp)
	{
		OSVERSIONINFOEX osvi;
		DWORDLONG dwlConditionMask = 0;

		// Initialize the OSVERSIONINFOEX structure.

		ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		osvi.dwMajorVersion = major;
		osvi.dwMinorVersion = minor;
		osvi.wServicePackMajor = sp;

		// Initialize the condition mask.

		VER_SET_CONDITION( dwlConditionMask, VER_MAJORVERSION, 
			VER_GREATER_EQUAL );
		VER_SET_CONDITION( dwlConditionMask, VER_MINORVERSION, 
			VER_GREATER_EQUAL );
		VER_SET_CONDITION( dwlConditionMask, VER_SERVICEPACKMAJOR, 
			VER_GREATER_EQUAL );

		// Perform the test.

		return VerifyVersionInfo(
			&osvi, 
			VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR,
			dwlConditionMask);
	}
	inline BOOL Is_WinXP_SP1_or_Later () 
	{
		return VerifyVersion(5, 1, 1);
	}
	inline BOOL Is_Win7_SP1_or_Later () 
	{
		return VerifyVersion(6, 1, 1);
	}

}

namespace Detours
{
}

namespace Hook
{
}

namespace Thread
{
}

#ifndef _WIN64
#ifdef WOW64_INCLUDE
namespace bootldr
{
}
#endif
#endif

namespace Process
{
}

namespace CodePage
{
	//////////////////////////////////////////////////////////////////////////
	// Code Page
#define CodePage_Japanese				932 // 3A4
#define CodePage_ChineseSimplified		936 // 3A8
#define CodePage_Korean					949 // 3B5
#define CodePage_ChineseTraditional		950 // 3B6

	//////////////////////////////////////////////////////////////////////////
	// ANSI to Unicode
	wstring ANSIToUnicode( const string& str );
	// Unicode to ANSI
	string UnicodeToANSI( const wstring& str );
	// UTF-8 to Unicode
	wstring UTF8ToUnicode( const string& str );
	// Unicode to UTF-8
	string UnicodeToUTF8( const wstring& str );
	// ANSI to UTF-8
	string ANSIToUTF8( const string& str );
	// UTF-8 to ANSI
	string UTF8ToANSI( const string& str );

	//////////////////////////////////////////////////////////////////////////
	// Gbk 转换成 Unicode
	wstring GbkToUnicode(LPCSTR szGBString);

	// Unicode 转换成 Gbk
	string UnicodeToGbk(LPCWSTR szUnicodeString);

	//////////////////////////////////////////////////////////////////////////
	// BIG5 转换成 Unicode
	wstring Big5ToUnicode(LPCSTR szBIG5String);

	// Unicode 转换成 BIG5
	string UnicodeToBig5(LPCWSTR szUnicodeString);

	//////////////////////////////////////////////////////////////////////////
	// JP 转换成 Unicode
	wstring JpToUnicode(LPCSTR szJPString);

	// Unicode 转换成 JP
	string UnicodeToJp(LPCWSTR szUnicodeString);

	//////////////////////////////////////////////////////////////////////////
	// KR 转换成 Unicode
	wstring KrToUnicode(LPCSTR szKRString);

	// Unicode 转换成 KR
	string UnicodeToKr(LPCWSTR szUnicodeString);

	//////////////////////////////////////////////////////////////////////////
	// 繁体中文BIG5 转换成 简体中文 Gbk
	string Big5ToGbk(LPCSTR szBIG5String, BOOL bTrans=FALSE);
	string Big5ToGbk(const string& str, BOOL bTrans=FALSE);

	// 简体中文 Gbk 转换成 繁体中文BIG5
	string GbkToBig5(LPCSTR szGBString, BOOL bTrans=TRUE);
	string GbkToBig5(const string& str, BOOL bTrans=TRUE);

	//////////////////////////////////////////////////////////////////////////
	// 日文 转换成 简体中文 Gbk
	string JpToGbk(LPCSTR szJPString, BOOL bTrans=FALSE);
	string JpToGbk(const string& str, BOOL bTrans=FALSE);

	// 简体中文 Gbk 转换成 日文
	string GbkToJp(LPCSTR szGBString, BOOL bTrans=TRUE);
	string GbkToJp(const string& str, BOOL bTrans=TRUE);
	
	//////////////////////////////////////////////////////////////////////////
	// 韩文 转换成 简体中文 Gbk
	string KrToGbk(LPCSTR szKRString, BOOL bTrans=FALSE);
	string KrToGbk(const string& str, BOOL bTrans=FALSE);

	// 简体中文 Gbk 转换成 韩文
	string GbkToKr(LPCSTR szGBString, BOOL bTrans=TRUE);
	string GbkToKr(const string& str, BOOL bTrans=TRUE);
}

namespace HtmlUtil
{
#ifdef __AFXWIN_H__ // MFC requires
#ifdef DEVELOPMENT_PLATFORM_VS
#ifndef _WIN64

	CStringA htmlDecode(CStringA& text);

#endif
#endif
#endif
}

namespace String
{
	//////////////////////////////////////////////////////////////////////////
	// 有关字符串的一些功能函数
	//class string_utils
	//{
	//public:
		// string与wstring转换
		std::wstring string2wstring(const std::string &str, std::wstring &wstr);

		// wstring与string转换
		std::string wstring2string(const std::wstring &wstr, std::string &str);

		// 数据流转换为十六进制格式字符串，默认小写
		TString Stream2HexTString(const VOID* data, size_t len, bool uppercase=false);
		TString Stream2HexTString(const vector<UCHAR>& hexval, bool uppercase=false);

		// 数据流转换为十六进制格式字符串，默认小写，没空格
		std::string to_hex_string(const VOID* data, size_t len, bool uppercase=false, bool space=false);
		std::string to_hex_string(const vector<UCHAR>& vec, bool uppercase=false, bool space=false);

		std::string to_hex_string_short(const VOID* data, size_t len, bool hex=true);
		std::string to_hex_string_ushort(const VOID* data, size_t len, bool hex=true);
		std::string to_hex_string_long(const VOID* data, size_t len, bool hex=true);
		std::string to_hex_string_ulong(const VOID* data, size_t len, bool hex=true);
		std::string to_hex_string_longlong(const VOID* data, size_t len, bool hex=true);
		std::string to_hex_string_ulonglong(const VOID* data, size_t len, bool hex=true);
#ifdef WOW64_INCLUDE
		std::string to_hex_string64(__int64 data, size_t len, bool uppercase=false, bool space=false);
#endif

		// 十六进制格式字符串转换为数据流，space指定源字符串是否有空格间隔
		std::string HexTString2Stream(const TString &str, bool space=false);
		std::string from_hex_string(const std::string& hex, bool space=false);
	//};
	
	bool GetXMLFirstTagValue(const std::string& xmlStr, const std::string& tag, std::string& tagValue);

#ifdef __AFXWIN_H__ // MFC requires
	//////////////////////////////////////////////////////////////////////////
	// 拆分字符串
	INT_PTR StrToK(CStringArray& sa, LPCTSTR str, LPCTSTR seps = TEXT(" ,	;"));
	INT_PTR StrToK_A(CStringArray& sa, LPCSTR str, LPCSTR seps = " ,	;");
#endif

	//////////////////////////////////////////////////////////////////////////
	// 字符串转大写
	std::string ToUpperA(std::string& strSrc);
	std::wstring ToUpperW(std::wstring& strSrc);
#ifdef UNICODE
#define ToUpper ToUpperW
#else
#define ToUpper ToUpperA
#endif

	//////////////////////////////////////////////////////////////////////////
	// 字符串转小写
	std::string ToLowerA(std::string& strSrc);
	std::wstring ToLowerW(std::wstring& strSrc);
#ifdef UNICODE
#define ToLower ToLowerW
#else
#define ToLower ToLowerA
#endif

	//////////////////////////////////////////////////////////////////////////
	// aes解密字符串
	// strText: 由aes cbf加密的字符串
	std::string DecryptString(std::string strText);
}

#ifdef __AFXWIN_H__ // MFC requires
namespace Msg
{
}

namespace Gdi
{
}

#else
#pragma chMSG(非MFC编译环境将禁用某些功能)
#endif // __AFXWIN_H__

namespace ShellCode
{
}

#ifdef __AFXWIN_H__ // MFC requires
namespace Controls
{
}
#endif


//////////////////////////////////////////////////////////////////////////
// 需要本机安装matlab
#ifdef INCLUDE_MATLAB
#include "engine.h"
// libmx.lib libmat.lib libeng.lib
#pragma comment(lib, "libmx.lib")
#pragma comment(lib, "libmat.lib")
#pragma comment(lib, "libeng.lib")
// engOpen, engClose
#endif

#ifdef INCLUDE_MATCOM

#include "matlib.h"
#pragma comment(lib, "v4501v.lib")

//#include "CStatic/ColorCStatic.h"

namespace Matcom
{
}
#endif


namespace Colors
{
}

namespace Search {
	namespace Sunday {
	}
}

//////////////////////////////////////////////////////////////////////////
// Windows Management Instrumentation (WMI)取系统信息
namespace WMI {
}


#if (_MSC_VER >= 800)

#if _MSC_VER >= 1200
//#pragma warning(pop)
#else
#pragma warning(default:4001)
#pragma warning(default:4201)
#pragma warning(default:4214)
/* Leave 4514 disabled.  It's an unneeded warning anyway. */
#pragma warning(default:4089) // all references to "xxx.dll" discarded by /OPT:REF
#pragma warning(default:4101) // 未引用的局部变量
#pragma warning(default:4102) // 未引用的标签
#pragma warning(default:4311) // 'variable' : pointer truncation from 'type' to 'type'
#pragma warning(default:4312) // 'operation' : conversion from 'type1' to 'type2' of greater size
#pragma warning(default:4414) // short jump to function converted to near
#pragma warning(default:4731) // 框架指针寄存器“ebp”被内联程序集代码修改
#pragma warning(default:4733) // 内联 asm 分配到“FS:0”: 处理程序未注册为安全处理程序
#pragma warning(default:4748) // 由于函数中禁用了优化功能，/GS 不能防止参数和局部变量造成本地缓冲区溢出
//#pragma warning(default:4819) // 该文件包含不能在当前代码页(936)中表示的字符。请将该文件保存为 Unicode 格式以防止数据丢失
#pragma warning(default:4995) // 名称被标记为 #pragma deprecated
#pragma warning(default:4996) // 被声明为已否决
#endif
#endif

#pragma data_seg()
#endif // _COMMON_HEADER_
