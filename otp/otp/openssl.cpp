#include "StdAfx.h"

#include "openssl.h"

#pragma data_seg(".d2")
namespace openssl
{
	//////////////////////////////////////////////////////////////////////////
	// 
	static int ui_open(UI *ui)
	{
		return UI_method_get_opener(UI_OpenSSL())(ui);
	}

	static int ui_read(UI *ui, UI_STRING *uis)
	{
		if (UI_get_input_flags(uis) & UI_INPUT_FLAG_DEFAULT_PWD	&& UI_get0_user_data(ui))
		{
			switch (UI_get_string_type(uis))
			{
			case UIT_PROMPT:
			case UIT_VERIFY:
				{
					const char *password =
						(const char*)(((PW_CB_DATA *)UI_get0_user_data(ui))->password);
					if (password && password[0] != '\0') {
						UI_set_result(ui, uis, password);
						return 1;
					}
				}
			default:
				break;
			}
		}
		return UI_method_get_reader(UI_OpenSSL())(ui, uis);
	}

	static int ui_write(UI *ui, UI_STRING *uis)
	{
		if (UI_get_input_flags(uis) & UI_INPUT_FLAG_DEFAULT_PWD && UI_get0_user_data(ui))
		{
			switch (UI_get_string_type(uis))
			{
			case UIT_PROMPT:
			case UIT_VERIFY:
				{
					const char *password =
						(const char*)(((PW_CB_DATA *)UI_get0_user_data(ui))->password);
					if (password && password[0] != '\0')
						return 1;
				}
			default:
				break;
			}
		}
		return UI_method_get_writer(UI_OpenSSL())(ui, uis);
	}

	static int ui_close(UI *ui)
	{
		return UI_method_get_closer(UI_OpenSSL())(ui);
	}

	UI_METHOD * setup_ui_method(void)
	{
		UI_METHOD *ui_method = NULL;
		ui_method = UI_create_method("OpenSSL application user interface");
		UI_method_set_opener(ui_method, ui_open);
		UI_method_set_reader(ui_method, ui_read);
		UI_method_set_writer(ui_method, ui_write);
		UI_method_set_closer(ui_method, ui_close);
		return ui_method;
	}

	void destroy_ui_method(UI_METHOD *ui_method)
	{
		if (ui_method) {
			UI_destroy_method(ui_method);
			ui_method = NULL;
		}
	}

	int password_callback(char *buf, int bufsiz, int verify, PW_CB_DATA *cb_tmp)
	{
		//int len;
		//char *tmp = "123456";

		///* We'd probably do something else if 'rwflag' is 1 */
		//printf("Enter pass phrase for \"%s\"\n", (char *)u);

		///* get pass phrase, length 'len' into 'tmp' */
		//len = (int)strlen(tmp);
		//if (len <= 0)
		//	return 0;

		//if (len > bufsiz)
		//	len = bufsiz;
		//memcpy(buf, tmp, len);
		//return len;


		UI *ui = NULL;
		int res = 0;
		const char *prompt_info = NULL;
		const char *password = NULL;
		PW_CB_DATA *cb_data = (PW_CB_DATA *)cb_tmp;

		UI_METHOD *ui_method = setup_ui_method();

		do 
		{
			if (cb_data) {
				if (cb_data->password)
					password = (const char*)cb_data->password;
				if (cb_data->prompt_info)
					prompt_info = cb_data->prompt_info;
			}

			if (password) {
				res = (int)strlen(password);
				if (res > bufsiz)
					res = bufsiz;
				memcpy(buf, password, res);
				break;
			}

			ui = UI_new_method(ui_method);
			if (ui) {
				int ok = 0;
				char *buff = NULL;
				int ui_flags = 0;
				char *prompt = NULL;

				prompt = UI_construct_prompt(ui, "pass phrase", prompt_info);

				ui_flags |= UI_INPUT_FLAG_DEFAULT_PWD;
				UI_ctrl(ui, UI_CTRL_PRINT_ERRORS, 1, 0, 0);

				if (ok >= 0)
					ok = UI_add_input_string(ui, prompt, ui_flags, buf,
					PW_MIN_LENGTH, bufsiz - 1);
				if (ok >= 0 && verify) {
					buff = (char *)OPENSSL_malloc(bufsiz);
					ok = UI_add_verify_string(ui, prompt, ui_flags, buff,
						PW_MIN_LENGTH, bufsiz - 1, buf);
				}
				if (ok >= 0)
					do {
						ok = UI_process(ui);
					}
					while (ok < 0 && UI_ctrl(ui, UI_CTRL_IS_REDOABLE, 0, 0, 0));

					if (buff) {
						OPENSSL_cleanse(buff, (unsigned int)bufsiz);
						OPENSSL_free(buff);
					}

					if (ok >= 0)
						res = (int)strlen(buf);
					if (ok == -1) {
						//BIO_printf(bio_err, "User interface error\n");
						//ERR_print_errors(bio_err);
						OPENSSL_cleanse(buf, (unsigned int)bufsiz);
						res = 0;
					}
					if (ok == -2) {
						//BIO_printf(bio_err, "aborted!\n");
						OPENSSL_cleanse(buf, (unsigned int)bufsiz);
						res = 0;
					}
					UI_free(ui);
					OPENSSL_free(prompt);
			}
		} while (0);

		destroy_ui_method(ui_method);

		return res;
	}


	namespace cipher
	{
		//////////////////////////////////////////////////////////////////////////
		// Base64
		string Base64::Encode(const string& input, bool with_new_line)
		{
			VMBEGIN2("openssl::cipher::Base64::Encode");

			BIO * b64 = NULL;
			BIO * bmem = NULL;
			BUF_MEM * bptr = NULL;

			b64 = BIO_new(BIO_f_base64());
			if(!with_new_line) {
				BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
			}
			bmem = BIO_new(BIO_s_mem());
			b64 = BIO_push(b64, bmem);

			BIO_write(b64, input.c_str(), (int)input.length());
			BIO_flush(b64);
			BIO_get_mem_ptr(b64, &bptr);
			//BIO_set_close(b64, BIO_NOCLOSE);

			string strout;
			strout.resize(bptr->length);
			CopyMemory((void*)strout.data(), bptr->data, bptr->length);

			//BIO_free(bmem); // 会使rsa读取公钥的BIO_new失败？
			//BIO_free(b64);

			BIO_free_all(b64);

			VMEND;

			return strout;
		}

		string Base64::Decode(const string& input, bool with_new_line)
		{
			VMBEGIN2("openssl::cipher::Base64::Decode");

			BIO * b64 = NULL;
			BIO * bmem = NULL;

			b64 = BIO_new(BIO_f_base64());
			if(!with_new_line) {
				BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
			}
			bmem = BIO_new_mem_buf((void*)input.c_str(), (int)input.length());
			bmem = BIO_push(b64, bmem);

			string strout;
			strout.resize(input.length());
			BIO_read(bmem, (void*)strout.data(), (int)input.length());

			//BIO_free(bmem); // 会使rsa读取公钥的BIO_new失败？
			//BIO_free(b64);

			BIO_free_all(b64);

			VMEND;

			return strout;
		}


		//////////////////////////////////////////////////////////////////////////
		// md5
		string md5::Md5Stream(const UCHAR* text, size_t len)
		{
			VMBEGIN2("openssl::cipher::md5::Md5Stream");

			const size_t n = MD5_DIGEST_LENGTH;

			// 调用md5哈希 
			UCHAR mdStr[n] = {0};
			MD5((const UCHAR*)text, len, mdStr);

			// 字节流存到string
			string strStream;
			Common::StreamToString(mdStr, n, strStream);

			VMEND;

			return strStream;
		}

		string md5::Md5Text(const string& text, bool uppercase /* = false */)
		{
			VMBEGIN2("openssl::cipher::md5::Md5Text");

			const size_t n = MD5_DIGEST_LENGTH;
			UNREFERENCED_PARAMETER(n);

			string strStream, strHash;
			// md5 stream
			strStream = Md5Stream((const UCHAR*)text.c_str(), text.length());
			// md5 string
			Common::StreamToString(strStream, strHash, uppercase);

			VMEND;

			return strHash;
		}


		//////////////////////////////////////////////////////////////////////////
		// sha_t
		//template<ALG_ID algid>
		//////////////////////////////////////////////////////////////////////////


		//////////////////////////////////////////////////////////////////////////
		// des 字节流加密/解密，ecb模式
		string des::EncryptStream_ecb(const UCHAR* Stream, size_t len, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::des::EncryptStream_ecb");

			DES_cblock* pkeyEncrypt;
			int desBlockLen = sizeof(DES_cblock);

			int keylen = (int)Key.length();
			if (keylen < desBlockLen)
			{
				keylen = desBlockLen;
			}
			pkeyEncrypt = (DES_cblock*)malloc(keylen);
			ZeroMemory(pkeyEncrypt, keylen);

			// 构造补齐后的密钥  
			//if (Key.length() <= 8)
			//	CopyMemory(pkeyEncrypt, Key.c_str(), Key.length());
			//else
			//	CopyMemory(pkeyEncrypt, Key.c_str(), 8);
			CopyMemory(pkeyEncrypt, Key.c_str(), keylen);

			// 密钥置换  
			DES_key_schedule keySchedule;
			DES_set_key_unchecked(pkeyEncrypt, &keySchedule);
			free(pkeyEncrypt);

			// 循环加密，每8字节一次  
			const_DES_cblock inputText;
			DES_cblock outputText;

			vector<UCHAR> vec;
			vec.clear();

			div_t dt = div((int)len, (int)desBlockLen);

			for (int i = 0; i < dt.quot; i++)
			{
				ZeroMemory(&inputText, sizeof(inputText));
				CopyMemory(&inputText, Stream + i * desBlockLen, desBlockLen);
				DES_ecb_encrypt(&inputText, &outputText, &keySchedule, enc);

				for (int j = 0; j < desBlockLen; j++)
					vec.push_back(*((UCHAR*)outputText+j));
			}

			if (dt.rem != 0)
			{
				ZeroMemory(&inputText, sizeof(inputText));
				CopyMemory(&inputText, Stream + dt.quot*desBlockLen, dt.rem);
				DES_ecb_encrypt(&inputText, &outputText, &keySchedule, enc);

				for (int j = 0; j < desBlockLen; j++)
					vec.push_back(*((UCHAR*)outputText+j));
			}

			string strStream;
			Common::StreamVectorToString(vec, strStream);

			VMEND;

			return strStream;
		}

		//////////////////////////////////////////////////////////////////////////
		// des 字符串加密/解密，ecb模式
		string des::EncryptText_ecb(const string& Text, const string& Key, int enc, bool uppercase /* = false */)
		{
			VMBEGIN2("openssl::cipher::des::EncryptText_ecb");

			string plainText;
			string strStream;
			string strText; // 密文/明文

			switch (enc)
			{
			case DES_ENCRYPT:
				plainText = Text;
				strStream = EncryptStream_ecb((UCHAR*)plainText.c_str(), plainText.length(), Key, enc);
				Common::StreamToString(strStream, strText, uppercase);
				break;

			case DES_DECRYPT:
				vector<UCHAR> vec;

				Common::StringToStreamVector(Text, vec);
				Common::StreamVectorToString(vec, plainText);
				strStream = EncryptStream_ecb((UCHAR*)plainText.c_str(), plainText.length(), Key, enc);
				strText = strStream;
				break;
			}

			VMEND;

			return strText;
		}



		//////////////////////////////////////////////////////////////////////////
		// aes 字节流加密/解密，ecb模式
		string aes::EncryptStream_ecb(const UCHAR* Stream, size_t len, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptStream_ecb");

			UCHAR buffer[AES_BLOCK_SIZE*8];
			ZeroMemory(buffer, sizeof(buffer));

			string strStream; // 密文/明文
			vector<UCHAR> vec;
			vec.clear();

			AES_KEY aeskey;
			int keybits = 256;

			if (AES_ENCRYPT == enc)
			{
				AES_set_encrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);
			}
			if (AES_DECRYPT == enc)
			{
				AES_set_decrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);
			}

			for(size_t i=0; i<len; i+=AES_BLOCK_SIZE)
			{
				AES_ecb_encrypt(Stream+i, buffer, &aeskey, enc);

				for (int j=0; j<AES_BLOCK_SIZE; j++)
				{
					vec.push_back(buffer[j]);
				}
			}

			Common::StreamVectorToString(vec, strStream);

			VMEND;

			return strStream;
		}
		
		//////////////////////////////////////////////////////////////////////////
		// aes 字节流加密/解密，cbc模式
		string aes::EncryptStream_cbc(const UCHAR* Stream, size_t len, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptStream_cbc");

			//note that iv length must be equal to AES_BLOCK_SIZE = 16-byte
			// {74DCDFE8-8B9A-4f3b-8183-E4E9EF79F296}
			static const UCHAR const_iv_cbc[AES_BLOCK_SIZE] = {
				0x74,0xdc,0xdf,0xe8,0x8b,0x9a,0x4f,0x3b,0x81,0x83,0xe4,0xe9,0xef,0x79,0xf2,0x96
			};
			UCHAR iv[AES_BLOCK_SIZE];
			CopyMemory(iv, const_iv_cbc, sizeof(const_iv_cbc));

			UCHAR buffer[AES_BLOCK_SIZE*8];
			ZeroMemory(buffer, sizeof(buffer));

			string strStream; // 密文/明文
			vector<UCHAR> vec;
			vec.clear();

			AES_KEY aeskey;
			int keybits = 256;

			if (AES_ENCRYPT == enc)
			{
				AES_set_encrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);
			}
			if (AES_DECRYPT == enc)
			{
				AES_set_decrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);
			}

			for(size_t i=0; i<len; i+=AES_BLOCK_SIZE)
			{
				AES_cbc_encrypt(Stream+i, buffer, AES_BLOCK_SIZE, &aeskey, iv, enc);

				for (int j=0; j<AES_BLOCK_SIZE; j++)
				{
					vec.push_back(buffer[j]);
				}
			}

			Common::StreamVectorToString(vec, strStream);

			VMEND;

			return strStream;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字节流加密/解密，cfb模式
		string aes::EncryptStream_cfb(const UCHAR* Stream, size_t len, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptStream_cfb");

			//note that iv length must be equal to AES_BLOCK_SIZE = 16-byte
			// {724BF8D4-D5E3-433d-A521-D8C6B347CFE1}
			static const UCHAR const_iv_cfb[AES_BLOCK_SIZE] = {
				0x72,0x4b,0xf8,0xd4,0xd5,0xe3,0x43,0x3d,0xa5,0x21,0xd8,0xc6,0xb3,0x47,0xcf,0xe1
			};
			UCHAR iv[AES_BLOCK_SIZE];
			CopyMemory(iv, const_iv_cfb, sizeof(const_iv_cfb));

			UCHAR buffer[AES_BLOCK_SIZE*8];
			ZeroMemory(buffer, sizeof(buffer));

			string strStream; // 密文/明文
			vector<UCHAR> vec(len);

			AES_KEY aeskey;
			int keybits = 128;

			AES_set_encrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);

			int num = 0;
			for(size_t index = 0; index < len; ++index) {
				AES_cfb128_encrypt(&Stream[index], &vec[index], 1, &aeskey, iv, &num, enc);
			}

			Common::StreamVectorToString(vec, strStream);

			VMEND;

			return strStream;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字节流加密/解密，ofb模式
		string aes::EncryptStream_ofb(const UCHAR* Stream, size_t len, const string& Key)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptStream_ofb");

			//note that iv length must be equal to AES_BLOCK_SIZE = 16-byte
			// {64CB4A43-87F3-436c-9D5E-2FE868C2DBEC}
			static const UCHAR const_iv_cfb[AES_BLOCK_SIZE] = {
				0x64,0xcb,0x4a,0x43,0x87,0xf3,0x43,0x6c,0x9d,0x5e,0x2f,0xe8,0x68,0xc2,0xdb,0xec
			};
			UCHAR iv[AES_BLOCK_SIZE];
			CopyMemory(iv, const_iv_cfb, sizeof(const_iv_cfb));

			UCHAR buffer[AES_BLOCK_SIZE*8];
			ZeroMemory(buffer, sizeof(buffer));

			string strStream; // 密文/明文
			vector<UCHAR> vec(len);

			AES_KEY aeskey;
			int keybits = 128;

			AES_set_encrypt_key((UCHAR*)Key.c_str(), keybits, &aeskey);

			int num = 0;
			for(size_t index = 0; index < len; ++index) {
				AES_ofb128_encrypt(&Stream[index], &vec[index], 1, &aeskey, iv, &num);
			}

			Common::StreamVectorToString(vec, strStream);

			VMEND;

			return strStream;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字符串加密/解密，ecb模式
		string aes::EncryptText_ecb(const string& Text, const string& Key, int enc, bool uppercase/* = false*/)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptText_ecb");

			string strText = EncryptText(Text, Key, enc, AES_ECB, uppercase);

			VMEND;

			return strText;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字符串加密/解密，cbc模式
		string aes::EncryptText_cbc(const string& Text, const string& Key, int enc, bool uppercase/* = false*/)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptText_cbc");

			string strText = EncryptText(Text, Key, enc, AES_CBC, uppercase);

			VMEND;

			return strText;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字符串加密/解密，cfb模式
		string aes::EncryptText_cfb(const string& Text, const string& Key, int enc, bool uppercase/* = false*/)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptText_cfb");

			string strText = EncryptText(Text, Key, enc, AES_CFB, uppercase);

			VMEND;

			return strText;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字符串加密/解密，ofb模式
		string aes::EncryptText_ofb(const string& Text, const string& Key, bool uppercase/* = false*/)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptText_ofb");

			string strText = EncryptText(Text, Key, AES_OFB, uppercase);

			VMEND;

			return strText;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 字符串加密/解密
		string aes::EncryptText(const string& Text, const string& Key, int enc, int mode /*= AES_CBC*/, bool uppercase /* = false */)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptText");

			string plainText; // 明文
			if (AES_ENCRYPT == enc)
			{
				plainText = Text;
			}
			if (AES_DECRYPT == enc)
			{
				vector<UCHAR> vec;

				Common::StringToStreamVector(Text, vec);
				Common::StreamVectorToString(vec, plainText);
			}

			string strStream;
			switch (mode)
			{
			case AES_ECB:
				strStream = EncryptStream_ecb((UCHAR*)plainText.c_str(), plainText.length(), Key, enc);
				break;
				
			case AES_CBC:
				strStream = EncryptStream_cbc((UCHAR*)plainText.c_str(), plainText.length(), Key, enc);
				break;
				
			case AES_CFB:
				strStream = EncryptStream_cfb((UCHAR*)plainText.c_str(), plainText.length(), Key, enc);
				break;

			case AES_OFB:
				strStream = EncryptStream_ofb((UCHAR*)plainText.c_str(), plainText.length(), Key);
				break;
			}

			string strText; // 密文/明文
			if (AES_ENCRYPT == enc)
			{
				Common::StreamToString(strStream, strText, uppercase);
			}
			if (AES_DECRYPT == enc)
			{
				strText = strStream;
			}

			VMEND;

			return strText;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 文件加密/解密
		bool aes::EncryptFile_ecb(__in LPCTSTR lpFileName, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptFile_ecb");

			bool r = EncryptFile(lpFileName, Key, enc, AES_ECB);
			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 文件加密/解密
		bool aes::EncryptFile_cbc(__in LPCTSTR lpFileName, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptFile_cbc");

			bool r = EncryptFile(lpFileName, Key, enc, AES_CBC);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 文件加密/解密
		bool aes::EncryptFile_cfb(__in LPCTSTR lpFileName, const string& Key, int enc)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptFile_cfb");

			bool r = EncryptFile(lpFileName, Key, enc, AES_CFB);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 文件加密/解密
		bool aes::EncryptFile_ofb(__in LPCTSTR lpFileName, const string& Key)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptFile_ofb");

			bool r = EncryptFile(lpFileName, Key, 0, AES_CFB);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// aes 文件加密/解密，各模式统一
		bool aes::EncryptFile(__in LPCTSTR lpFileName, const string& Key, int enc, int mode)
		{
			VMBEGIN2("openssl::cipher::aes::EncryptFile");

			bool r = false;

#ifdef __AFXWIN_H__ // MFC requires
			do 
			{
				string strText, EnAES;
				TString strEncryptFile;

				CStdioFile sf;
				if (!sf.Open(lpFileName, CFile::modeRead|CFile::typeBinary))
				{
					OPENSSL_ERROR_MSG;
					break;
				}

				strText.resize((size_t)sf.GetLength());
				sf.Read((VOID*)strText.data(), (UINT)sf.GetLength());
				sf.Close();

				switch (mode)
				{
				case AES_ECB:
					EnAES = EncryptStream_ecb((UCHAR*)strText.data(), strText.length(), Key, enc);
					break;
					
				case AES_CBC:
					EnAES = EncryptStream_cbc((UCHAR*)strText.data(), strText.length(), Key, enc);
					break;

				case AES_CFB:
					EnAES = EncryptStream_cfb((UCHAR*)strText.data(), strText.length(), Key, enc);
					break;

				case AES_OFB:
					EnAES = EncryptStream_ofb((UCHAR*)strText.data(), strText.length(), Key);
					break;
				}

				strEncryptFile = lpFileName;
				if (AES_ENCRYPT == enc)
				{
					strEncryptFile += TEXT(".aes");
				}
				if (AES_DECRYPT == enc)
				{
					strEncryptFile += TEXT(".aes.txt");
				}

				sf.Open(strEncryptFile.c_str(), CFile::modeCreate|CFile::modeReadWrite|CFile::typeBinary);
				sf.Write(EnAES.data(), (UINT)EnAES.length());
				sf.Close();

				r = true;
			} while (0);
#endif
			VMEND;

			return r;
		}



		//////////////////////////////////////////////////////////////////////////
		// rsa 生成密钥对
		bool rsa::GenerateKeypair(int Bits /* = 2048 */, int KeyType /* = RSA_PUBKEY_PUBLIC */, bool bSavetoFile /* = true */)
		{
			VMBEGIN2("openssl::cipher::rsa::GenerateKeypair");

			bool r = false;

			do 
			{
				// Key要256 bit以上
				if (Bits < 256)
				{
					Bits = 256;
				}
				
				//The pseudo-random number generator must be seeded prior to calling RSA_generate_key_ex
				UCHAR seed[] = {0xb9, 0x1a, 0x72, 0x5b, 0x43, 0x4e, 0xab, 0xf7};
				RAND_seed(seed, sizeof(seed));
				RSA* rsa = RSA_new();//allocate empty key
				ULONG e = RSA_F4 ; //65537 public exponent
				BIGNUM* bne = BN_new(); // allocate BINNUM structure in heap
				BN_set_word(bne, e); //store that public exponent in big-number object bne

				RSA_generate_key_ex(rsa, Bits, bne, NULL);
				if (NULL == rsa)
				{
					OPENSSL_ERROR_MSG
					break;
				}

				if (bSavetoFile)
				{
					//create a bio file object and write private key to it
					BIO* bio_file_private = BIO_new_file(RSA_PRIKEY_FILE, "w+b");
					// -----BEGIN RSA PRIVATE KEY-----
					PEM_write_bio_RSAPrivateKey(bio_file_private, rsa, NULL, NULL, 0, NULL, NULL);

					//create a bio file object and write public key to it
					BIO* bio_file_public = BIO_new_file(RSA_PUBKEY_FILE, "w+b");

					// 公钥文件2种不同的标记头，保存和读取的方法有些不同
					// #1 -----BEGIN RSA PUBLIC KEY-----
					if (RSA_PUBKEY_RSA_PUBLIC == KeyType)
					{
						PEM_write_bio_RSAPublicKey(bio_file_public, rsa);
					}

					// #2 -----BEGIN PUBLIC KEY-----
					if (RSA_PUBKEY_PUBLIC == KeyType)
					{
						PEM_write_bio_RSA_PUBKEY(bio_file_public, rsa);
					}

					// 第一种方式，保存到文件
					//allocate in-memory bio
					BIO* bio_mem_private = BIO_new(BIO_s_mem());
					BIO* bio_mem_public = BIO_new(BIO_s_mem());
					//extract private and public key to bio-object respectively
					PEM_write_bio_RSAPrivateKey(bio_mem_private, rsa, NULL, NULL, 0, NULL, NULL);
					if (RSA_PUBKEY_RSA_PUBLIC == KeyType)
					{
						PEM_write_bio_RSAPublicKey(bio_mem_public, rsa);
					}
					if (RSA_PUBKEY_PUBLIC == KeyType)
					{
						PEM_write_bio_RSA_PUBKEY(bio_mem_public, rsa);
					}

					//BIO_pending function return number of byte read to bio buffer during previous step
					int private_len = BIO_pending(bio_mem_private);
					int public_len = BIO_pending(bio_mem_public);

					//this two buffer will hold the keys string
					string strPubkey, strPrikey;
					strPrikey.resize(private_len);
					strPubkey.resize(public_len);

					//copy extracted keys to buffer 
					BIO_read(bio_mem_private, (VOID*)strPrikey.c_str(), private_len);
					BIO_read(bio_mem_public, (VOID*)strPubkey.c_str(), public_len);

					////display the result on screen
					//DbgStrA("prikey:\n%s", m_strPrivatekey.c_str());
					//DbgStrA("pubkey:\n%s", m_strPublickey.c_str());
					SetPrikeyFromString(strPrikey);
					SetPubkeyFromString(strPubkey, KeyType);

					//clean up
					BIO_free_all(bio_mem_private);
					BIO_free_all(bio_mem_public);
					BIO_free_all(bio_file_private);
					BIO_free_all(bio_file_public);


					// 第二种方式，保存到文件
					//FILE *file;

					//// 私钥
					//file = fopen(RSA_PRI_KEY_FILE, "wb");
					//if (NULL == file )
					//{
					//	OPENSSL_ERROR_MSG
					//		break;
					//}

					//// -----BEGIN RSA PRIVATE KEY-----
					//PEM_write_RSAPrivateKey(file, rsa, NULL, NULL, keybits, NULL, NULL);
					//fclose(file);

					//// 公钥
					//file = fopen(RSA_PUB_KEY_FILE, "wb");
					//if (NULL == file )
					//{
					//	OPENSSL_ERROR_MSG
					//		break;
					//}

					//// 公钥文件2种不同的标记头，保存和读取的方法有些不同
					//// #1 -----BEGIN RSA PUBLIC KEY-----
					////PEM_write_RSAPublicKey(file, rsa);

					//// #2 -----BEGIN PUBLIC KEY-----
					//PEM_write_RSA_PUBKEY(file, rsa);

					//fclose(file);
				}

				//clean up memory
				BN_free(bne);
				RSA_free(rsa);
				RAND_cleanup();

				r = true;
			} while (0);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// rsa 从文件读取公钥
		bool rsa::SetPubkeyFromFile(const string& strPubkeyFile, int KeyType)
		{
			VMBEGIN2("openssl::cipher::rsa::SetPubkeyFromFile");

			bool r = false;

			do 
			{
				if (!Common::IsFileExistA(strPubkeyFile))
				{
					Output::DbgPrintA("%s 文件不存在", strPubkeyFile.c_str());
					break;
				}

				RSA* rsa = NULL; //RSA_new();//allocate empty key
				BIO *key = NULL;

				// bio公钥文件
				key = BIO_new_file(strPubkeyFile.c_str(), "rb");
				BIO* bio_mem_key = BIO_new(BIO_s_mem());

				// 公钥文件2种不同的标记头，保存和读取的方法有些不同
				// #1 -----BEGIN RSA PUBLIC KEY-----
				if (RSA_PUBKEY_RSA_PUBLIC == KeyType)
				{
					rsa = PEM_read_bio_RSAPublicKey(key, &rsa, NULL, NULL);
					PEM_write_bio_RSAPublicKey(bio_mem_key, rsa);
				}
				// #2 -----BEGIN PUBLIC KEY-----
				if (RSA_PUBKEY_PUBLIC == KeyType)
				{
					rsa = PEM_read_bio_RSA_PUBKEY(key, &rsa, NULL, NULL);
					PEM_write_bio_RSA_PUBKEY(bio_mem_key, rsa);
				}

				int key_len = BIO_pending(bio_mem_key);
				string strkey;
				strkey.resize(key_len);
				BIO_read(bio_mem_key, (VOID*)strkey.c_str(), key_len);

				SetPubkeyFromString(strkey, KeyType);

				//key = BIO_new(BIO_s_file());
				//BIO_read_filename(key, strPubkeyFile.c_str());

				// 公钥文件2种不同的标记头，保存和读取的方法有些不同
				// #1 -----BEGIN RSA PUBLIC KEY-----
				// #2 -----BEGIN PUBLIC KEY-----
				//rsa = PEM_read_bio_RSAPublicKey(key, &rsa, NULL, NULL); //#1 ok, #2 ok
				//rsa = PEM_read_bio_RSA_PUBKEY(key, &rsa, NULL, NULL); //#1 ok, #2 error

				BIO_free_all(bio_mem_key);
				BIO_free_all(key);
				RSA_free(rsa);
				
				r = true;
			} while (0);

			VMEND;

			return r;

		}

		//////////////////////////////////////////////////////////////////////////
		// rsa 从文件读取私钥
		bool rsa::SetPrikeyFromFile(const string& strPrikeyFile)
		{
			VMBEGIN2("openssl::cipher::rsa::SetPrikeyFromFile");

			bool r = false;

			do 
			{
				if (!Common::IsFileExistA(strPrikeyFile))
				{
					Output::DbgPrintA("%s 文件不存在", strPrikeyFile.c_str());
					break;
				}

				RSA* rsa = NULL; //RSA_new();//allocate empty key
				BIO *key = NULL;

				// bio私钥文件
				key = BIO_new_file(strPrikeyFile.c_str(), "rb");
				rsa = PEM_read_bio_RSAPrivateKey(key, &rsa, NULL, NULL);
				
				BIO* bio_mem_key = BIO_new(BIO_s_mem());
				PEM_write_bio_RSAPrivateKey(bio_mem_key, rsa, NULL, NULL, 0, NULL, NULL);

				int key_len = BIO_pending(bio_mem_key);
				string strkey;
				strkey.resize(key_len);
				BIO_read(bio_mem_key, (VOID*)strkey.c_str(), key_len);

				SetPrikeyFromString(strkey);

				//key = BIO_new(BIO_s_file());
				//BIO_read_filename(key, strPrikeyFile.c_str());

				//// -----BEGIN RSA PRIVATE KEY-----
				//rsa = PEM_read_bio_RSAPrivateKey(key, &rsa, NULL, NULL);

				BIO_free_all(bio_mem_key);
				BIO_free_all(key);
				RSA_free(rsa);

				r = true;
			} while (0);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// rsa 从内存buffer读取公钥
		bool rsa::SetPubkeyFromString(const string& strPubkey, int KeyType)
		{
			VMBEGIN2("openssl::cipher::rsa::SetPubkeyFromString");

			bool r = false;

			do 
			{
				if (strPubkey.length() <= 0)
				{
					break;
				}

				m_strPublickey = strPubkey;
				m_nPubkeyType = KeyType;

				//RSA* rsa = RSA_new();//allocate empty key
				////load string key to bio object
				//BIO* mem_bio;

				//// 读取公钥
				//mem_bio = BIO_new_mem_buf((VOID*)strPubkey.c_str(), (INT)strPubkey.length());
				//rsa = PEM_read_bio_RSAPublicKey(mem_bio, &rsa, NULL, NULL);
				//BIO_free(mem_bio);
				//RSA_free(rsa);

				r = true;

			} while (0);

			VMEND;

			return r;
		}

		//////////////////////////////////////////////////////////////////////////
		// rsa 从内存buffer读取私钥
		bool rsa::SetPrikeyFromString(const string& strPrikey)
		{
			VMBEGIN2("openssl::cipher::rsa::SetPrikeyFromString");

			bool r = false;

			do 
			{
				if (strPrikey.length() <= 0)
				{
					break;
				}

				m_strPrivatekey = strPrikey;

				//RSA* rsa = RSA_new();//allocate empty key
				////load string key to bio object
				//BIO* mem_bio;

				//// 读取私钥
				//mem_bio = BIO_new_mem_buf((VOID*)strPrikey.c_str(), (INT)strPrikey.length());
				//rsa = PEM_read_bio_RSAPrivateKey(mem_bio, &rsa, NULL, NULL);
				//BIO_free(mem_bio);
				//RSA_free(rsa);

				r = true;

			} while (0);

			VMEND;

			return r;
		}
		

		//////////////////////////////////////////////////////////////////////////
		// rsa 字节流加密/解密
		string rsa::EncryptStream(const UCHAR* Stream, size_t len, int enc)
		{
			VMBEGIN2("openssl::cipher::rsa::EncryptStream");

			string strStream;

			do 
			{
				//if (!RSA_check_key(m_rsa))
				//{
				//	break;
				//}

				RSA* rsa = NULL; //RSA_new();
				BIO* mem_bio = NULL;

				typedef int (*fnRSA_public_encrypt)(int flen, const unsigned char *from,
                       unsigned char *to, RSA *rsa, int padding);
				fnRSA_public_encrypt fnRsa = NULL;

				bool bKeySetOk = true;

				switch (enc)
				{
				case RSA_PUBKEY_ENCRYPT:
					if (m_strPublickey.length() <= 10)
					{
						bKeySetOk = false;
						break;
					}
					fnRsa = RSA_public_encrypt;
					mem_bio = BIO_new_mem_buf((VOID*)m_strPublickey.c_str(), (INT)m_strPublickey.length());
					if (RSA_PUBKEY_PUBLIC == m_nPubkeyType)
					{
						rsa = PEM_read_bio_RSA_PUBKEY(mem_bio, &rsa, NULL, NULL);
					}
					if (RSA_PUBKEY_RSA_PUBLIC == m_nPubkeyType)
					{
						rsa = PEM_read_bio_RSAPublicKey(mem_bio, &rsa, NULL, NULL);
					}
					break;

				case RSA_PRIKEY_DECRYPT:
					if (m_strPrivatekey.length() <= 10)
					{
						bKeySetOk = false;
						break;
					}
					fnRsa = RSA_private_decrypt;
					mem_bio = BIO_new_mem_buf((VOID*)m_strPrivatekey.c_str(), (INT)m_strPrivatekey.length());
					rsa = PEM_read_bio_RSAPrivateKey(mem_bio, &rsa, NULL, NULL);
					break;

				case RSA_PRIKEY_ENCRYPT:
					if (m_strPrivatekey.length() <= 10)
					{
						bKeySetOk = false;
						break;
					}
					fnRsa = RSA_private_encrypt;
					mem_bio = BIO_new_mem_buf((VOID*)m_strPrivatekey.c_str(), (INT)m_strPrivatekey.length());
					rsa = PEM_read_bio_RSAPrivateKey(mem_bio, &rsa, NULL, NULL);
					break;

				case RSA_PUBKEY_DECRYPT:
					if (m_strPublickey.length() <= 10)
					{
						bKeySetOk = false;
						break;
					}
					fnRsa = RSA_public_decrypt;
					mem_bio = BIO_new_mem_buf((VOID*)m_strPublickey.c_str(), (INT)m_strPublickey.length());
					if (RSA_PUBKEY_PUBLIC == m_nPubkeyType)
					{
						rsa = PEM_read_bio_RSA_PUBKEY(mem_bio, &rsa, NULL, NULL);
					}
					if (RSA_PUBKEY_RSA_PUBLIC == m_nPubkeyType)
					{
						rsa = PEM_read_bio_RSAPublicKey(mem_bio, &rsa, NULL, NULL);
					}
					break;
				}

				if (bKeySetOk == false)
				{
					Output::DbgPrintA("RSA key 设置不正确??");
					break;
				}

				strStream.resize(RSA_size(rsa));

				// 加密函数
				int ret = fnRsa(
					(int)len,
					(const UCHAR*)Stream,
					(UCHAR*)strStream.data(),
					rsa,
					RSA_PKCS1_PADDING);

				strStream.resize(ret);

				// cleanup
				BIO_free_all(mem_bio);
				RSA_free(rsa);
			} while (0);

			VMEND;

			return strStream;
		}

		//////////////////////////////////////////////////////////////////////////
		// rsa 字符串加密/解密
		string rsa::EncryptText(const string& Text, int enc, bool uppercase /* = false */)
		{
			VMBEGIN2("openssl::cipher::rsa::EncryptText");

			string plainText; // 明文
			string strStream;
			string strText; // 密文

			switch (enc)
			{
			case RSA_PUBKEY_ENCRYPT:
			case RSA_PRIKEY_ENCRYPT:
				plainText = Text;
				strStream = EncryptStream((UCHAR*)plainText.c_str(), plainText.length(), enc);
				strText = String::to_hex_string((UCHAR*)strStream.data(), strStream.length(), uppercase);
				break;

			case RSA_PRIKEY_DECRYPT:
			case RSA_PUBKEY_DECRYPT:
				plainText = String::from_hex_string(Text);
				strStream = EncryptStream((UCHAR*)plainText.c_str(), plainText.length(), enc);
				strText = strStream;
				break;
			}

			VMEND;

			return strText;
		}
	}
}

#pragma data_seg()