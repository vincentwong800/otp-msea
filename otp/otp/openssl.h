#pragma once

#include <WinCrypt.h> // ALG_ID

#include <openssl/md5.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/rsa.h>
#include <openssl/des.h>
#include <openssl/aes.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/applink.c> // 1.0.2o
#include <openssl/bn.h>
#include <openssl/rand.h>
#include <openssl/x509.h>
//#include <openssl/x509v3.h>
#include <openssl/pkcs12.h>
#include <openssl/ui.h>
#include <openssl/ssl.h>
#include <openssl/opensslv.h>
#include <openssl//blowfish.h>

#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "Crypt32.lib")
#pragma comment(lib, "advapi32.lib")

#if _MSC_VER > 1200 //vc6+
#ifdef _WIN64
#ifdef _DEBUG
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\debug\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\debug\\lib\\ssleay32.lib")
#else
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\release\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\win64\\release\\lib\\ssleay32.lib")
#endif
#else
#ifdef _DEBUG
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\debug\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\debug\\lib\\ssleay32.lib")
#else
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\release\\lib\\libeay32.lib")
#pragma comment(lib, "C:\\usr\\local\\vs\\ssl\\release\\lib\\ssleay32.lib")
#endif
#endif
#else
#ifdef _DEBUG
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#else
#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib")
#endif
#endif


//#pragma comment(lib, "libeay32.lib") // 1.0.2o
//#pragma comment(lib, "ssleay32.lib")
//#pragma comment(lib, "libcrypto.lib") // 1.1.1d
//#pragma comment(lib, "libssl.lib")

namespace openssl
{
#define OPENSSL_ERROR_MSG	\
	Output::DbgPrint(TEXT("%s"), Output::GetFormattedMessageString().c_str());

# define PW_MIN_LENGTH 4
	typedef struct pw_cb_data {
		const void *password;
		const char *prompt_info;
	} PW_CB_DATA;

	int password_callback(char *buf, int bufsiz, int verify, PW_CB_DATA *cb_tmp);

	inline VOID OpensslRelease()
	{
		//CONF_modules_unload(1);			//for conf
		//EVP_cleanup();					//For EVP
		//ENGINE_cleanup();				//for engine
		CRYPTO_cleanup_all_ex_data();	//generic
		//ERR_remove_state(0);			//for ERR
		//ERR_free_strings();				//for ERR
	}

	class MySSLInfo
	{
	public:
		MySSLInfo()
		{
			CHAR buff[KB] = {0};
			//Output::DbgPrintBufferA(buff, KB, "OPENSSL_VERSION_NUMBER: %p", OPENSSL_VERSION_NUMBER);
			//Output::DbgPrintBufferA(buff, KB, "SSLeay(): %p", SSLeay());
			Output::DbgPrintBufferA(buff, KB, "SSLeay_version(SSLEAY_VERSION):  %s", SSLeay_version(SSLEAY_VERSION));
			Output::DbgPrintBufferA(buff, KB, "SSLeay_version(SSLEAY_CFLAGS):   %s", SSLeay_version(SSLEAY_CFLAGS));
			Output::DbgPrintBufferA(buff, KB, "SSLeay_version(SSLEAY_BUILT_ON): %s", SSLeay_version(SSLEAY_BUILT_ON));
			Output::DbgPrintBufferA(buff, KB, "SSLeay_version(SSLEAY_PLATFORM): %s", SSLeay_version(SSLEAY_PLATFORM));
			//Output::DbgPrintBufferA(buff, KB, "SSLeay_version(SSLEAY_DIR): %s", SSLeay_version(SSLEAY_DIR));

			//SSL_CTX* ctx = NULL;
			//SSL*     ssl = NULL;
			//ctx = SSL_CTX_new(SSLv23_client_method());
			//ssl = SSL_new(ctx);
			//if (ssl)
			//{
			//	Output::DbgPrintBufferA(buff, KB, "SSL_get_version: %s", SSL_get_version(ssl));
			//}
			//SSL_free(ssl);
			//SSL_CTX_free(ctx);
		}
		~MySSLInfo()
		{
			OpensslRelease();
		}
	};
	static MySSLInfo g_MySSLInfo;


	namespace cipher
	{
		//////////////////////////////////////////////////////////////////////////
		// Base64
		class Base64
		{
		public:
			//Base64(){};
			static string Encode(const string& input, bool with_new_line = false);
			static string Decode(const string& input, bool with_new_line = false);
		};


		//////////////////////////////////////////////////////////////////////////
		// evp hash 模块类
		typedef const EVP_MD *(*fnEVP_EVP_MD)(void);

		template<VOID* t_HashName>
		class evp_hash
		{
		public:
			evp_hash(){}
			~evp_hash(){ OpensslRelease(); }
		public:
			// EVP接口
			string StreamEVP(const UCHAR* text, size_t len)
			{
				VMBEGIN2("openssl::cipher::evp_hash::StreamEVP");

				string strStream;

				UINT md_len = EVP_MAX_MD_SIZE;
				UCHAR mdStr[EVP_MAX_MD_SIZE] = {0};

				EVP_MD_CTX *mdctx = EVP_MD_CTX_create();
				EVP_DigestInit_ex(mdctx, ((fnEVP_EVP_MD)t_HashName)(), NULL);
				EVP_DigestUpdate(mdctx, text, len);
				EVP_DigestFinal_ex(mdctx, mdStr, &md_len);
				EVP_MD_CTX_destroy(mdctx);

				// 字节流存到string
				Common::StreamToString(mdStr, md_len, strStream);

				VMEND;

				return strStream;
			}

			string TextEVP(const string& text, bool uppercase = false)
			{
				VMBEGIN2("openssl::cipher::evp_hash::TextEVP");

				const size_t n = MD5_DIGEST_LENGTH;

				string strStream, strHash;
				// md5 stream
				strStream = StreamEVP((const UCHAR*)text.c_str(), text.length());
				// md5 string
				Common::StreamToString(strStream, strHash, uppercase);

				VMEND;

				return strHash;
			}

		};

		typedef evp_hash<EVP_sha1> t_evp_sha1;
		typedef evp_hash<EVP_sha224> t_evp_sha224;
		typedef evp_hash<EVP_sha256> t_evp_sha256;
		typedef evp_hash<EVP_sha384> t_evp_sha384;
		typedef evp_hash<EVP_sha512> t_evp_sha512;
		typedef evp_hash<EVP_md4> t_evp_md4;
		typedef evp_hash<EVP_md5> t_evp_md5;
		typedef evp_hash<EVP_mdc2> t_evp_mdc2;
		typedef evp_hash<EVP_ripemd160> t_evp_rmd160;
		typedef evp_hash<EVP_whirlpool> t_evp_whirlpool;


		//////////////////////////////////////////////////////////////////////////
		// md5
		class md5
		{
		public:
			md5() {}
			~md5() { OpensslRelease();}
		public:
			// md5字节流hash
			string Md5Stream(const UCHAR* text, size_t len);

			//md5字符串格式hash，uppercase指定结果的大小写
			string Md5Text(const string& text, bool uppercase = false);
		};


		//////////////////////////////////////////////////////////////////////////
		// sha
		template<ALG_ID algid>
		class sha_t
		{
		public:
			sha_t(){}
			~sha_t(){ OpensslRelease();}
		public:
			// sha字节流hash，返回结果的长度
			string ShaStream(const UCHAR* text, size_t len)
			{
				VMBEGIN2("openssl::cipher::sha_t::ShaStream");

				string strHash;
				size_t n = 0;

				typedef unsigned char *(*fnSHA)(const unsigned char*, size_t, unsigned char*);
				fnSHA fnsha;

				switch (algid)
				{
					//case CALG_SHA:
					//n = SHA_DIGEST_LENGTH;
					//fnsha = SHA;
					//break;
				case CALG_SHA1:
					n = SHA_DIGEST_LENGTH;
					fnsha = SHA1;
					break;
				case CALG_SHA_256:
					n = SHA256_DIGEST_LENGTH;
					fnsha = SHA256;
					break;
				case CALG_SHA_384:
					n = SHA384_DIGEST_LENGTH;
					fnsha = SHA384;
					break;
				case CALG_SHA_512:
					n = SHA512_DIGEST_LENGTH;
					fnsha = SHA512;
					break;
				}
				if (n)
				{
					strHash.resize(n);
					fnsha(text, len, (UCHAR*)strHash.data());
				}

				VMEND;

				return strHash;
			}

			// sha字符串格式hash，uppercase指定结果的大小写
			string ShaText(const string& text, bool uppercase = false)
			{
				VMBEGIN2("openssl::cipher::sha_t::ShaText");

				std::stringstream sstr;
				string strHash;
				strHash = ShaStream((const UCHAR*)text.c_str(), text.length());
				if (strHash.length())
				{
					sstr.setf(std::ios_base::hex, std::ios::basefield);
					if(uppercase)
						sstr.setf(std::ios_base::uppercase);

					sstr.fill('0');

					for (size_t i=0; i<strHash.length(); i++)
					{
						sstr << std::setw(2) << (UINT)(UCHAR)(strHash[i]);
					}
				}

				VMEND;

				return sstr.str();
			}
		};

		typedef sha_t<CALG_SHA>			t_sha;
		typedef sha_t<CALG_SHA1>		t_sha1;
		typedef sha_t<CALG_SHA_256>		t_sha256;
		typedef sha_t<CALG_SHA_384>		t_sha384;
		typedef sha_t<CALG_SHA_512>		t_sha512;


		//////////////////////////////////////////////////////////////////////////
		// des, Data Encrytion Standard（数据加密标准）
		class des
		{
		public:
			des(){}
			~des(){ OpensslRelease();}
		public:
			// 字节流加密/解密，ECB模式
			string EncryptStream_ecb(const UCHAR* Stream, size_t len, const string& Key, int enc);
			// 字符串加密/解密，ECB模式
			string EncryptText_ecb(const string& Text, const string& Key, int enc, bool uppercase = false);
		};


		//////////////////////////////////////////////////////////////////////////
		// aes, Advanced Encrytion Standard（高级加密标准）
		class aes
		{
		public:
			aes(){}
			~aes(){ OpensslRelease();}
		public:
			
			// ECB模式（电子密码本模式：Electronic codebook）
			// CBC模式（密码分组链接：Cipher-block chaining）
			// CFB模式（密文反馈：Cipher feedback）
			// OFB模式（输出反馈：Output feedback）
#define AES_ECB			1
#define AES_CBC			2
#define AES_CFB			3
#define AES_OFB			4

			// 字节流加密/解密，ECB
			string EncryptStream_ecb(const UCHAR* Stream, size_t len, const string& Key, int enc);
			// 字节流加密/解密，CBC
			string EncryptStream_cbc(const UCHAR* Stream, size_t len, const string& Key, int enc);
			// 字节流加密/解密，CFB
			string EncryptStream_cfb(const UCHAR* Stream, size_t len, const string& Key, int enc);
			// 字节流加密/解密，OFB
			string EncryptStream_ofb(const UCHAR* Stream, size_t len, const string& Key);

			// 字符串加密/解密，ECB
			string EncryptText_ecb(const string& Text, const string& Key, int enc, bool uppercase = false);
			// 字符串加密/解密，CBC
			string EncryptText_cbc(const string& Text, const string& Key, int enc, bool uppercase = false);
			// 字符串加密/解密，CFB
			string EncryptText_cfb(const string& Text, const string& Key, int enc, bool uppercase = false);
			// 字符串加密/解密，OFB
			string EncryptText_ofb(const string& Text, const string& Key, bool uppercase = false);

			// 文件加密/解密，ECB，加密增加后缀名：.aes； 解密增加后缀名：.aes.txt
			bool EncryptFile_ecb(__in LPCTSTR lpFileName, const string& Key, int enc);
			// 文件加密/解密，CBC，加密增加后缀名：.aes； 解密增加后缀名：.aes.txt
			bool EncryptFile_cbc(__in LPCTSTR lpFileName, const string& Key, int enc);
			// 文件加密/解密，CFB，加密增加后缀名：.aes； 解密增加后缀名：.aes.txt
			bool EncryptFile_cfb(__in LPCTSTR lpFileName, const string& Key, int enc);
			// 文件加密/解密，OFB，加密增加后缀名：.aes； 解密增加后缀名：.aes.txt
			bool EncryptFile_ofb(__in LPCTSTR lpFileName, const string& Key);


			// 字符串加密/解密，各模式统一
			string EncryptText(const string& Text, const string& Key, int enc, int mode = AES_CBC, bool uppercase = false);

			// 文件加密/解密，各模式统一
			bool EncryptFile(__in LPCTSTR lpFileName, const string& Key, int enc, int mode = AES_CFB);
		};


		//////////////////////////////////////////////////////////////////////////
		// rsa
		class rsa
		{
# define RSA_PUBKEY_ENCRYPT     1
# define RSA_PRIKEY_DECRYPT     2
# define RSA_PRIKEY_ENCRYPT     3
# define RSA_PUBKEY_DECRYPT     4

#define RSA_PRIKEY_FILE "prikey.pem"    // 私钥名
#define RSA_PUBKEY_FILE "pubkey.pem"    // 公钥名

#define RSA_PUBKEY_PUBLIC		1 // -----BEGIN PUBLIC KEY-----
#define RSA_PUBKEY_RSA_PUBLIC	2 // -----BEGIN RSA PUBLIC KEY-----

		private:
			string m_strPublickey;
			string m_strPrivatekey;
			int m_nPubkeyType;
			//RSA *m_rsa;

		public:
			rsa() {	/*m_rsa = RSA_new();*/ }
			~rsa() {
				/*RSA_free(m_rsa);*/
				OpensslRelease();
			}

			string GetPubKey() const {return m_strPublickey;}
			string GetPriKey() const {return m_strPrivatekey;}
			int GetPubkeyType() const {return m_nPubkeyType;}

		public:

			// 生成密钥对，并使用此密钥对，默认是2048bit密钥
			// openssl genrsa -out prikey.pem 1024
			// openssl rsa -in prikey.pem -pubout -out pubkey.pem
			bool GenerateKeypair(int Bits = 2048, int KeyType = RSA_PUBKEY_PUBLIC, bool bSavetoFile = true);

			// 从文件读取公钥，文件必须要全路径
			bool SetPubkeyFromFile(const string& strPubkeyFile, int KeyType);
			// 从文件读取私钥，文件必须要全路径
			bool SetPrikeyFromFile(const string& strPrikeyFile);
			// 从文件读取密钥对，文件必须要全路径
			bool SetKeypairFromFile(const string& strPubkeyFile, int KeyType, const string& strPrikeyFile)
			{
				bool r = true;
				r &= SetPubkeyFromFile(strPubkeyFile, KeyType);
				r &= SetPrikeyFromFile(strPrikeyFile);
				return r;
			}

			// 从内存buffer读取公钥
			bool SetPubkeyFromString(const string& strPubkey, int KeyType);
			// 从内存buffer读取私钥
			bool SetPrikeyFromString(const string& strPrikey);
			// 从内存buffer读取密钥对
			bool SetKeypairFromString(const string& strPubkey, int KeyType, const string& strPrikey)
			{
				bool r = true;
				r &= SetPubkeyFromString(strPubkey, KeyType);
				r &= SetPrikeyFromString(strPrikey);
				return r;
			}

			// 字节流加密/解密
			string EncryptStream(const UCHAR* Stream, size_t len, int enc);
			//// 字节流私钥加密/公钥解密
			//string EncryptStreamPrikey(const UCHAR* Stream, size_t len, int enc);

			// 字符串加密/解密
			string EncryptText(const string& Text, int enc, bool uppercase = false);
			//// 字符串私钥加密/公钥解密
			//string EncryptTextPrikey(const string& Text, int enc, bool uppercase = false);
		};
	}
}