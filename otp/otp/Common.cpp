#include "StdAfx.h"

#include "Common.h"

#pragma data_seg(".d3")

#ifdef STACKTRACER_HEADER
void HandleExcept()
{
	std::string str = StackTracer::GetExceptionMsg();

	CString cs(str.c_str());
	OutputDebugString(cs);

#ifdef EXCEPTION_LOGFILE
	BOOL b = FALSE;

	do 
	{
		try
		{
			TString strFile = SELOGFILE;
			CAdoLogger adoLogger(strFile);

			if (!Common::IsFileExist(strFile))
			{
//				Output::DbgPrint(TEXT("%s 不存在，创建.."), strFile.c_str());
				// 创建指定的数据库文件(.mdb)
				adoLogger.CreateDb();
			}

			adoLogger.OpenDb();

			// 创建表，如有需要
			if (!adoLogger.IsTableExist())
			{
//				Output::DbgPrintA("异常数据表不存在，创建..");
				adoLogger.CreateTable2();
			}

			CString sText(str.c_str());
			sText.Replace(_T("\r\n"), _T(";"));
			//OutputDebugString(sText);

			// 写一条log
			CString strLog;
			strLog.Format(TEXT("INSERT INTO %s (eTime,eMsg) VALUES (Now(),'%s')"), adoLogger.m_strExpTable, sText);
			adoLogger.WriteLog(strLog);

			adoLogger.CloseDb();
			b = TRUE;
		} CATCH_COM();
	} while (0);
#endif
}
#endif

namespace Output
{
	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，MaxBufferLength：400k bytes
	HRESULT WINAPIV DbgPrintA(LPCSTR lpszFmt, ...)
	{
#ifdef DEVELOPMENT_PLATFORM_VS
		std::string strResult;
		if (NULL != lpszFmt)
		{
			va_list marker = NULL;
			va_start(marker, lpszFmt); //初始化变量参数

			size_t nLength = _vscprintf(lpszFmt, marker) + 2*sizeof(CHAR); //获取格式化字符串长度
			nLength *= sizeof(CHAR);

			std::vector<CHAR> vBuffer(nLength, '\0'); //创建用于存储格式化字符串的字符数组

			int nWritten = StringCchVPrintfA(&vBuffer[0], vBuffer.size(), lpszFmt, marker);
			//if (nWritten > 0)
			{
				strResult = &vBuffer[0];
			}
			va_end(marker); //重置变量参数
		}
		if (!strResult.empty())
		{
#ifdef DEBUGSTRING_TO_FILE
			SYSTEMTIME st;
			::GetLocalTime(&st);
			CHAR szt[100] = {0};
			StringCchPrintfA(szt, 100, "[%02d/%02d %02d:%02d:%02d.%03d]", st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
			std::string sout = szt;
			sout.append(strResult.c_str());
			std::cout<<sout<<endl;
			Undocument::g_Global.of.flush();
#else
			::OutputDebugStringA(strResult.c_str());
#endif
		}
		return S_OK;

#else

		va_list marker;

		// combine output string and variables
		va_start(marker, lpszFmt);

		HRESULT hr = StringCchVPrintfA(Undocument::g_Global.fmtFileA, DbgStrBufferLengthA, lpszFmt, marker);

		va_end(marker);

		::OutputDebugStringA(Undocument::g_Global.fmtFileA);

		return S_OK;
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，MaxBufferLength：400k bytes
	HRESULT WINAPIV DbgPrintW(LPCWSTR lpszFmt, ...)
	{
		va_list marker;

		// combine output string and variables
		va_start(marker, lpszFmt);

		HRESULT hr = StringCchVPrintfW(Undocument::g_Global.fmtFileW, DbgStrBufferLengthW, lpszFmt, marker);

		va_end(marker);

		::OutputDebugStringW(Undocument::g_Global.fmtFileW);
		return S_OK;

		//std::wstring strResult;
		//if (NULL != lpszFmt)
		//{
		//	va_list marker = NULL;
		//	va_start(marker, lpszFmt); //初始化变量参数

		//	size_t nLength = vwprintf_s(lpszFmt, marker) + 2*sizeof(WCHAR); //获取格式化字符串长度
		//	nLength *= sizeof(WCHAR);

		//	std::vector<WCHAR> vBuffer(nLength, L'\0'); //创建用于存储格式化字符串的字符数组

		//	int nWritten = StringCchVPrintfW(&vBuffer[0], vBuffer.size(), lpszFmt, marker);
		//	//if (nWritten > 0)
		//	{
		//		strResult = &vBuffer[0];
		//	}
		//	va_end(marker); //重置变量参数
		//}
		//if (!strResult.empty())
		//{
		//	::OutputDebugStringW(strResult.c_str());
		//}
		return S_OK;
	}
	
	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，需提供buff,len参数
	HRESULT WINAPIV DbgPrintBufferA(LPSTR buff, size_t len, LPCSTR lpszFmt, ...)
	{
		va_list marker;

		// combine output string and variables
		va_start(marker, lpszFmt);

		HRESULT hr = StringCchVPrintfA(buff, len, lpszFmt, marker);

		va_end(marker);

		::OutputDebugStringA(buff);
		return hr;
	}

	//////////////////////////////////////////////////////////////////////////
	// 调试输出，增强的OutputDebugString，需提供buff,len参数
	HRESULT WINAPIV DbgPrintBufferW(LPWSTR buff, size_t len, LPCWSTR lpszFmt, ...)
	{
		va_list marker;

		// combine output string and variables
		va_start(marker, lpszFmt);

		HRESULT hr = StringCchVPrintfW(buff, len, lpszFmt, marker);

		va_end(marker);

		::OutputDebugStringW(buff);
		return hr;
	}

	//////////////////////////////////////////////////////////////////////////
	// 根据当前的错误号，获取其错误字符串
	LPTSTR GetFormattedMessage(LPTSTR szText)
	{
		LPVOID lpMsgBuf;
		DWORD ErrCode = GetLastError();
		TCHAR buff[MAX_PATH];

		::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			ErrCode,
			0,
			(LPTSTR)&lpMsgBuf,
			0,
			NULL);

		StringCchPrintf(buff, MAX_PATH-1, TEXT("错误号 %d(0x%p)：%s"), ErrCode, ErrCode, (LPTSTR)lpMsgBuf);

		StringCchCopy(szText, MAX_PATH, buff);

		::LocalFree(lpMsgBuf);

		return szText;
	}

	//////////////////////////////////////////////////////////////////////////
	// 根据当前的错误号，获取其错误字符串
	TString GetFormattedMessageString()
	{
		LPVOID lpMsgBuf;
		DWORD ErrCode = GetLastError();
		TCHAR buff[MAX_PATH];
		TString str;

		::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			ErrCode,
			0,
			(LPTSTR)&lpMsgBuf,
			0,
			NULL);

		StringCchPrintf(buff, MAX_PATH-1, TEXT("错误号 %d(0x%p)：%s"), ErrCode, ErrCode, (LPTSTR)lpMsgBuf);
		// 去除末尾的换行符
		if (buff[_tcslen(buff)-1] == 0x0a && buff[_tcslen(buff)-2] == 0x0d)
		{
			buff[_tcslen(buff)-2] = TCHAR(0);
		}
		str = buff;

		::LocalFree(lpMsgBuf);

		return str;
	}

	//////////////////////////////////////////////////////////////////////////
	// 根据指定的错误码，取其文本格式的错误字符串
	TString GetFormattedMessageStringByErrorNo(DWORD nError)
	{
		LPVOID lpMsgBuf;
		DWORD ErrCode = nError;
		TCHAR buff[MAX_PATH];
		TString str;

		::FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			ErrCode,
			0,
			(LPTSTR)&lpMsgBuf,
			0,
			NULL);

		StringCchPrintf(buff, MAX_PATH-1, TEXT("%s"), (LPTSTR)lpMsgBuf);
		// 去除末尾的换行符
		if (buff[_tcslen(buff)-1] == 0x0a && buff[_tcslen(buff)-2] == 0x0d)
		{
			buff[_tcslen(buff)-2] = TCHAR(0);
		}
		str = buff;

		::LocalFree(lpMsgBuf);

		return str;
	}

}

namespace Common
{
	//////////////////////////////////////////////////////////////////////////
	// in: 2,3; out:TEXT("2_3")
	LPCTSTR SetByte(INT n, INT d)
	{
		TCHAR buff[100];
		StringCchPrintf(buff, 32, TEXT("%d_%d"), n, (UCHAR)d);
		TString str = buff;
		//Output::DbgPrint(str.c_str());

		return str.c_str();
	}

	STRUCT(setbyte)
	{
		INT n;
		INT d;
	};

	//////////////////////////////////////////////////////////////////////////
	// 初始化数组，不作越界检测
	VOID InitData(UCHAR* data, ...)
	{
		//INT n,d;
		//UINT nPos = 0;
		//va_list marker;

		//va_start( marker, data );     /* Initialize variable arguments. */
		//do 
		//{
		//	n = va_arg(marker, INT);
		//	d = va_arg(marker, INT);
		//	//Output::DbgPrint(TEXT("%p,%p"), sb.n, sb.d);

		//	if (n>0 && n<100000 && d<256)
		//	{
		//		FillMemory(data+nPos, n, d);
		//		nPos += n;
		//	}
		//	else
		//		break;
		//} while (TRUE);
		//va_end( marker );              /* Reset variable arguments.      */

		vector<setbyte> vec_sb;
		vec_sb.clear();
		setbyte sb;

		va_list marker;

		va_start( marker, data );     /* Initialize variable arguments. */
		do 
		{
			sb.n = va_arg(marker, INT);
			sb.d = va_arg(marker, INT);
			//Output::DbgPrint(TEXT("%p,%p"), sb.n, sb.d);

			if (sb.n>0 && sb.n<0x10000 && sb.d<256)
				vec_sb.push_back(sb);
			else
				break;
		} while (TRUE);
		va_end( marker );              /* Reset variable arguments.      */

		UINT nPos = 0;
		vector<setbyte>::iterator psb = vec_sb.begin();
		while (psb != vec_sb.end())
		{
			//Output::DbgPrint(TEXT("%p,%02x"), psb->n, psb->d);
			FillMemory(data+nPos, psb->n, psb->d);
			nPos += psb->n;
			//
			psb++;
		}
	}

	// 从字符串中转换为UCHAR值
	// in:	"1a"
	// out:	0x1a
	UCHAR uCharFromStringA(CHAR* s)
	{
		UCHAR data[128] = {0};
		std::string str(s);
		if (str.length() >= 1*3-1)
		{
			for (int i=0; i<1; i++)
			{
				sscanf_s(&str[i*3], "%02x", &data[i]);
			}
		}
		return *(UCHAR*)data;
	}
	UCHAR uCharFromStringW(WCHAR* s)
	{
		UCHAR data[128] = {0};
		std::wstring str(s);
		if (str.length() >= 1*3-1)
		{
			for (int i=0; i<1; i++)
			{
				swscanf_s(&str[i*3], L"%02x", &data[i]);
			}
		}
		return *(UCHAR*)data;
	}

	// 从字符串中转换为SHORT值
	// in:	"1a 2b"
	// out:	0x2b1a
	USHORT uShortFromStringA(CHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::string str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 2*n-1)
		{
			for (int i=0; i<2; i++)
			{
				sscanf_s(&str[i*n], "%02x", &data[i]);
			}
		}
		return *(USHORT*)data;
	}
	USHORT uShortFromStringW(WCHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::wstring str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 2*n-1)
		{
			for (int i=0; i<2; i++)
			{
				swscanf_s(&str[i*n], L"%02x", &data[i]);
			}
		}
		return *(USHORT*)data;
	}

	// 从字符串中转换为LONG值
	// in:	"1a 2b d0 e1"
	// out:	0xe1d02b1a
	ULONG uLongFromStringA(CHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::string str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 4*n-1)
		{
			for (int i=0; i<4; i++)
			{
				sscanf_s(&str[i*n], "%02x", &data[i]);
			}
		}
		return *(ULONG*)data;
	}
	ULONG uLongFromStringW(WCHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::wstring str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 4*n-1)
		{
			for (int i=0; i<4; i++)
			{
				swscanf_s(&str[i*n], L"%02x", &data[i]);
			}
		}
		return *(ULONG*)data;
	}
	
	// 从字符串中转换为LONGLONG值
	// in:	"1a 2b d0 e1"
	// out:	0xe1d02b1a
	ULONGLONG uLongLongFromStringA(CHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::string str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 8*n-1)
		{
			for (int i=0; i<8; i++)
			{
				sscanf_s(&str[i*n], "%02x", &data[i]);
			}
		}
		return *(ULONGLONG*)data;
	}
	ULONGLONG uLongLongFromStringW(WCHAR* s, bool bSpace/*=true*/)
	{
		UCHAR data[128] = {0};
		std::wstring str(s);
		size_t n = 2;
		if (bSpace)
		{
			n = 3;
		}
		if (str.length() >= 8*n-1)
		{
			for (int i=0; i<8; i++)
			{
				swscanf_s(&str[i*n], L"%02x", &data[i]);
			}
		}
		return *(ULONGLONG*)data;
	}


}

namespace Undocument
{

}

namespace Detours
{
}

namespace Hook
{
}

namespace Thread
{
}

namespace Process
{
}

namespace CodePage
{
	//////////////////////////////////////////////////////////////////////////
	// 其他转Unicode
	wstring _ToUnicode(LPCSTR szString, UINT uCodePage)
	{
		UINT nCodePage = uCodePage;
		int nLength = MultiByteToWideChar(nCodePage, 0, szString, -1, NULL, 0);
		WCHAR* pBuffer = new WCHAR[nLength+1];
		MultiByteToWideChar(nCodePage, 0, szString, -1, pBuffer, nLength);
		pBuffer[nLength]=0;
		wstring wsString = pBuffer;
		delete[] pBuffer;
		return wsString;
	}

	//////////////////////////////////////////////////////////////////////////
	// Unicode转其他
	string UnicodeTo_(LPCWSTR wsString, UINT uCodePage)
	{
		UINT nCodePage = uCodePage;
		int nLength = WideCharToMultiByte(nCodePage, 0, wsString, -1,NULL, 0, NULL, NULL);
		CHAR* pBuffer = new CHAR[nLength+1];
		WideCharToMultiByte(nCodePage, 0, wsString, -1, pBuffer, nLength, NULL, NULL);
		pBuffer[nLength]=0;
		string sString = pBuffer;
		delete[] pBuffer;
		return sString;
	}

	//////////////////////////////////////////////////////////////////////////
	// ANSI to Unicode
	wstring ANSIToUnicode( const string& str )
	{
		size_t len = 0;
		len = str.length();
		int unicodeLen = ::MultiByteToWideChar( CP_ACP,	0, str.c_str(), -1, NULL, 0 );
		wchar_t * pUnicode;
		pUnicode = new wchar_t[unicodeLen+1];
		ZeroMemory(pUnicode, (unicodeLen+1)*sizeof(wchar_t));
		::MultiByteToWideChar( CP_ACP, 0, str.c_str(), -1, (LPWSTR)pUnicode, unicodeLen );
		wstring rt;
		rt = ( wchar_t* )pUnicode;
		delete[] pUnicode;
		return rt;
	}

	// Unicode to ANSI
	string UnicodeToANSI( const wstring& str )
	{
		char* pElementText;
		int iTextLen;
		iTextLen = WideCharToMultiByte( CP_ACP,	0, str.c_str(),	-1,	NULL, 0, NULL, NULL );
		pElementText = new char[iTextLen + 1];
		ZeroMemory(pElementText, (iTextLen+1)*sizeof(char));
		::WideCharToMultiByte( CP_ACP, 0, str.c_str(), -1, pElementText, iTextLen, NULL, NULL );
		string strText;
		strText = pElementText;
		delete[] pElementText;
		return strText;
	}

	// UTF-8 to Unicode
	wstring UTF8ToUnicode( const string& str )
	{
		size_t len = 0;
		len = str.length();
		int unicodeLen = ::MultiByteToWideChar( CP_UTF8, 0,	str.c_str(), -1, NULL, 0 );
		wchar_t * pUnicode;
		pUnicode = new wchar_t[unicodeLen+1];
		ZeroMemory(pUnicode, (unicodeLen+1)*sizeof(wchar_t));
		::MultiByteToWideChar( CP_UTF8, 0, str.c_str(), -1, (LPWSTR)pUnicode, unicodeLen );
		wstring rt;
		rt = ( wchar_t* )pUnicode;
		delete[] pUnicode;
		return rt;
	}

	// Unicode to UTF-8
	string UnicodeToUTF8( const wstring& str )
	{
		char*   pElementText;
		int iTextLen;
		iTextLen = WideCharToMultiByte( CP_UTF8, 0, str.c_str(), -1, NULL, 0, NULL, NULL );
		pElementText = new char[iTextLen + 1];
		ZeroMemory(pElementText, (iTextLen+1)*sizeof(char));
		::WideCharToMultiByte( CP_UTF8,	0, str.c_str(),	-1,	pElementText, iTextLen,	NULL, NULL );
		string strText;
		strText = pElementText;
		delete[] pElementText;
		return strText;
	}
	
	// ANSI to UTF-8
	string ANSIToUTF8( const string& str )
	{
		return UnicodeToUTF8(ANSIToUnicode(str));
	}

	// UTF-8 to ANSI
	string UTF8ToANSI( const string& str )
	{
		return UnicodeToANSI(UTF8ToUnicode(str));
	}

	//////////////////////////////////////////////////////////////////////////
	// Gbk 转换成 Unicode
	wstring GbkToUnicode(LPCSTR szGBString)
	{
		return _ToUnicode(szGBString, CodePage_ChineseSimplified);
	}

	// Unicode 转换成 Gbk
	string UnicodeToGbk(LPCWSTR szUnicodeString)
	{
		return UnicodeTo_(szUnicodeString, CodePage_ChineseSimplified);
	}

	//////////////////////////////////////////////////////////////////////////
	// BIG5 转换成 Unicode
	wstring Big5ToUnicode(LPCSTR szBIG5String)
	{
		return _ToUnicode(szBIG5String, CodePage_ChineseTraditional);
	}

	// Unicode 转换成 BIG5
	string UnicodeToBig5(LPCWSTR szUnicodeString)
	{
		return UnicodeTo_(szUnicodeString, CodePage_ChineseTraditional);
	}

	//////////////////////////////////////////////////////////////////////////
	// JP 转换成 Unicode
	wstring JpToUnicode(LPCSTR szJPString)
	{
		return _ToUnicode(szJPString, CodePage_Japanese);
	}

	// Unicode 转换成 JP
	string UnicodeToJp(LPCWSTR szUnicodeString)
	{
		return UnicodeTo_(szUnicodeString, CodePage_Japanese);
	}

	//////////////////////////////////////////////////////////////////////////
	// KR 转换成 Unicode
	wstring KrToUnicode(LPCSTR szKRString)
	{
		return _ToUnicode(szKRString, CodePage_Korean);
	}

	// Unicode 转换成 KR
	string UnicodeToKr(LPCWSTR szUnicodeString)
	{
		return UnicodeTo_(szUnicodeString, CodePage_Korean);
	}

	//////////////////////////////////////////////////////////////////////////
	// 繁体中文BIG5 转换成 简体中文 Gbk
	string Big5ToGbk(LPCSTR szBIG5String, BOOL bTrans)
	{
		wstring szUnicodeBuff = Big5ToUnicode(szBIG5String);
		string szGB2312Buff = UnicodeToGbk(szUnicodeBuff.c_str());
		if(FALSE == bTrans)
		{
			//delete[] szUnicodeBuff;
			return szGB2312Buff;
		}

		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength + 1];
		LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, pBuffer, nLength);
		pBuffer[nLength] = 0;
		string sString = pBuffer;
		delete[] pBuffer;
		return sString;

		//delete[] szUnicodeBuff;
		//delete[] szGB2312Buff;

		//return pBuffer;
	}
	string Big5ToGbk(const string& str, BOOL bTrans)
	{
		return Big5ToGbk(str.c_str(), bTrans);
	}

	// 简体中文 Gbk 转换成 繁体中文BIG5
	string GbkToBig5(LPCSTR szGBString, BOOL bTrans)
	{
		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength+1];
		LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, pBuffer, nLength);
		pBuffer[nLength]=0;
		if(FALSE == bTrans)
		{
			string sString = pBuffer;
			delete[] pBuffer;
			return sString;
			//return pBuffer;
		}

		wstring pUnicodeBuff = GbkToUnicode(pBuffer);
		string pBIG5Buff = UnicodeToBig5(pUnicodeBuff.c_str());

		delete[] pBuffer;
		//delete[] pUnicodeBuff;

		return pBIG5Buff;
	}
	string GbkToBig5(const string& str, BOOL bTrans)
	{
		return GbkToBig5(str.c_str(), bTrans);
	}

	//////////////////////////////////////////////////////////////////////////
	// 日文 转换成 简体中文 Gbk
	string JpToGbk(LPCSTR szJPString, BOOL bTrans)
	{
		wstring szUnicodeBuff = JpToUnicode(szJPString);
		string szGB2312Buff = UnicodeToGbk(szUnicodeBuff.c_str());
		if(FALSE == bTrans)
		{
			//delete[] szUnicodeBuff;
			return szGB2312Buff;
		}

		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength + 1];
		LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, pBuffer, nLength);
		pBuffer[nLength] = 0;
		string sSting = pBuffer;
		delete[] pBuffer;
		return sSting;

		//delete[] szUnicodeBuff;
		//delete[] szGB2312Buff;

		//return pBuffer;
	}
	string JpToGbk(const string& str, BOOL bTrans)
	{
		return JpToGbk(str.c_str(), bTrans);
	}

	// 简体中文 Gbk 转换成 日文
	string GbkToJp(LPCSTR szGBString, BOOL bTrans)
	{
		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength+1];
		LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, pBuffer, nLength);
		pBuffer[nLength]=0;
		if(FALSE == bTrans)
		{
			string sString = pBuffer;
			delete[] pBuffer;
			return sString;
			//return pBuffer;
		}

		wstring pUnicodeBuff = GbkToUnicode(pBuffer);
		string pJPBuff = UnicodeToJp(pUnicodeBuff.c_str());

		delete[] pBuffer;
		//delete[] pUnicodeBuff;

		return pJPBuff;
	}
	string GbkToJp(const string& str, BOOL bTrans)
	{
		return GbkToJp(str.c_str(), bTrans);
	}

	//////////////////////////////////////////////////////////////////////////
	// 韩文 转换成 简体中文 Gbk
	string KrToGbk(LPCSTR szKRString, BOOL bTrans)
	{
		wstring szUnicodeBuff = KrToUnicode(szKRString);
		string szGB2312Buff = UnicodeToGbk(szUnicodeBuff.c_str());
		if(FALSE == bTrans)
		{
			//delete[] szUnicodeBuff;
			return szGB2312Buff;
		}

		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength + 1];
		LCMapStringA(lcid, LCMAP_SIMPLIFIED_CHINESE, szGB2312Buff.c_str(), -1, pBuffer, nLength);
		pBuffer[nLength] = 0;
		string sSting = pBuffer;
		delete[] pBuffer;
		return sSting;

		//delete[] szUnicodeBuff;
		//delete[] szGB2312Buff;

		//return pBuffer;
	}
	string KrToGbk(const string& str, BOOL bTrans)
	{
		return KrToGbk(str.c_str(), bTrans);
	}

	// 简体中文 Gbk 转换成 韩文
	string GbkToKr(LPCSTR szGBString, BOOL bTrans)
	{
		LCID lcid = MAKELCID(MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED),
			SORT_CHINESE_PRC);

		int nLength = LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, NULL, 0);
		CHAR* pBuffer = new CHAR[nLength+1];
		LCMapStringA(lcid, LCMAP_TRADITIONAL_CHINESE, szGBString, -1, pBuffer, nLength);
		pBuffer[nLength]=0;
		if(FALSE == bTrans)
		{
			string sString = pBuffer;
			delete[] pBuffer;
			return sString;
			//return pBuffer;
		}

		wstring pUnicodeBuff = GbkToUnicode(pBuffer);
		string pJPBuff = UnicodeToKr(pUnicodeBuff.c_str());

		delete[] pBuffer;
		//delete[] pUnicodeBuff;

		return pJPBuff;
	}
	string GbkToKr(const string& str, BOOL bTrans)
	{
		return GbkToKr(str.c_str(), bTrans);
	}
}

namespace HtmlUtil
{
}

namespace String
{
	//////////////////////////////////////////////////////////////////////////
	// string与wstring转换
	wstring string2wstring(const string &str, wstring &wstr)
	{
		wstr = CodePage::ANSIToUnicode(str);
		return wstr;

		//int nLen = (int)str.length();
		//wstr.resize(nLen, L'w');

		//int nResult = MultiByteToWideChar(
		//	CP_ACP,
		//	0,
		//	(LPCSTR)str.c_str(),
		//	nLen,
		//	(LPWSTR)wstr.c_str(),
		//	nLen);

		//if (nResult == 0)
		//{
		//	return FALSE;
		//}

		//return TRUE;
	}

	// wstring与string转换
	std::string wstring2string(const wstring &wstr, std::string &str)
	{
		str = CodePage::UnicodeToANSI(wstr);
		return str;

		//int nLen = (int)wstr.length();    
		//str.resize(nLen, 's');

		//int nResult = WideCharToMultiByte(
		//	CP_ACP,
		//	0,
		//	(LPCWSTR)wstr.c_str(),
		//	nLen,
		//	(LPSTR)str.c_str(),
		//	nLen,
		//	NULL,
		//	NULL);

		//if (nResult == 0)
		//{
		//	return FALSE;
		//}

		//return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////
	// string_utils Stream2HexStr
	TString Stream2HexTString(const VOID* data, size_t len, bool uppercase/* =false */)
	{
		VMBEGIN2("String::string_utils::Stream2HexTString");

		string strHex = to_hex_string((VOID*)data, len, uppercase);
		TString t_strHex;

#ifdef UNICODE
		String::string2wstring(strHex, t_strHex);
#else
		t_strHex = strHex;
#endif

		VMEND;

		return t_strHex;
	}

	//////////////////////////////////////////////////////////////////////////
	// string_utils Stream2HexStr
	TString Stream2HexTString(const vector<unsigned char>& hexval, bool uppercase/* =false */)
	{
		VMBEGIN2("String::string_utils::Stream2HexTString2");

		string strHex = to_hex_string(hexval, uppercase);
		TString t_strHex;

#ifdef UNICODE
		String::string2wstring(strHex, t_strHex);
#else
		t_strHex = strHex;
#endif

		VMEND;

		return t_strHex;
	}

	//////////////////////////////////////////////////////////////////////////
	// string_utils HexStr2Stream
	std::string HexTString2Stream(const TString &str, bool space/* =false */)
	{
		VMBEGIN2("String::string_utils::HexTString2Stream");

		string strHex, strStream;

#ifdef UNICODE
		wstring2string(str, strHex);
#else
		strHex = str;
#endif
		strStream = from_hex_string(strHex, space);

		VMEND;

		return strStream;
	}

	//////////////////////////////////////////////////////////////////////////
	// 数据流转换为十六进制格式字符串，默认小写，没空格
	std::string to_hex_string(const vector<UCHAR>& vec, bool uppercase/* =false */, bool space/*=false*/)
	{
		VMBEGIN2("String::string_utils::to_hex_string");

		string strHex, strStream;
		Common::StreamVectorToString(vec, strStream);
		Common::StreamToString(strStream, strHex, uppercase);

		if (space)
		{
			string tmp1;
			tmp1.empty();
			for (size_t i=0; i<strHex.length(); i+=2)
			{
				string tmp2(strHex, i, 2);
				tmp1 += tmp2;
				tmp1 += " ";
			}
			strHex = tmp1;
		}

		VMEND;

		return strHex;
	}

	//////////////////////////////////////////////////////////////////////////
	// 数据流转换为十六进制格式字符串，默认小写，没空格
	std::string to_hex_string(const VOID* data, size_t len, bool uppercase/* =false */, bool space/*=false*/)
	{
		VMBEGIN2("String::string_utils::to_hex_string2");

		string strHex, strStream;
		Common::StreamToString((const UCHAR*)data, len, strStream);
		Common::StreamToString(strStream, strHex, uppercase);

		if (space)
		{
			string tmp1;
			tmp1.empty();
			for (size_t i=0; i<strHex.length(); i+=2)
			{
				string tmp2(strHex, i, 2);
				tmp1 += tmp2;
				tmp1 += " ";
			}
			strHex = tmp1;
		}

		VMEND;

		return strHex;
	}

	std::string to_hex_string_short(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_short");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=4)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			SHORT n = (SHORT)Common::uShortFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}
	std::string to_hex_string_ushort(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_ushort");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=4)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			USHORT n = (USHORT)Common::uShortFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}

	std::string to_hex_string_long(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_long");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=8)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			LONG n = (LONG)Common::uLongFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}
	std::string to_hex_string_ulong(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_ulong");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=8)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			ULONG n = (ULONG)Common::uLongFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}

	std::string to_hex_string_longlong(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_longlong");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=16)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			LONGLONG n = (LONGLONG)Common::uLongLongFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}
	std::string to_hex_string_ulonglong(const VOID* data, size_t len, bool hex/* =true */)
	{
		VMBEGIN2("String::string_utils::to_hex_string_ulonglong");

		string strHex = to_hex_string(data, len);
		string tmp1 = "";
		for (size_t i=0; i<strHex.length(); i+=16)
		{
			std::stringstream ss;
			ss.clear();
			std::string s = "";
			ULONGLONG n = (ULONGLONG)Common::uLongLongFromStringA((char*)strHex.c_str()+i, false);
			if (hex)
			{
				ss << std::hex;
			}
			ss << n;
			ss >> s;
			tmp1 += s;
			tmp1 += " ";
		}
		strHex = tmp1;

		VMEND;

		return strHex;
	}

#ifdef WOW64_INCLUDE
	std::string to_hex_string64(__int64 data, size_t len, bool uppercase/* =false */, bool space/*=false*/)
	{
		VMBEGIN2("String::string_utils::to_hex_string64");

		string strHex, strStream;
		UCHAR* pdata = (UCHAR*)malloc(len);
		SIZE_T rd;
		ReadProcessMemory64(GetCurrentProcess(), data, pdata, len, &rd);
		strHex = to_hex_string(pdata, len, uppercase, space);
		free(pdata);

		//Common::StreamToString64((__int64)data, len, strStream);
		//Common::StreamToString(strStream, strHex, uppercase);

		//if (space)
		//{
		//	string tmp1;
		//	tmp1.empty();
		//	for (size_t i=0; i<strHex.length(); i+=2)
		//	{
		//		string tmp2(strHex, i, 2);
		//		tmp1 += tmp2;
		//		tmp1 += " ";
		//	}
		//	strHex = tmp1;
		//}

		VMEND;

		return strHex;
	}
#endif

	//////////////////////////////////////////////////////////////////////////
	// 十六进制格式字符串转换为数据流，space指定源字符串是否有空格间隔
	std::string from_hex_string(const std::string& hex, bool space/* =false */)
	{
		VMBEGIN2("String::string_utils::from_hex_string");

		vector<UCHAR> vec;
		string strHex, strStream;

		int n = 2;
		if (space)
		{
			n = 3;
		}
		strHex.empty();
		for (size_t i=0; i<hex.length(); i+=n)
		{
			string tmp(hex, i, 2);
			strHex += tmp;
		}

		Common::StringToStreamVector(strHex, vec);
		Common::StreamVectorToString(vec, strStream);

		VMEND;

		return strStream;
	}

	//xmlStr = ....<tag>tagValue</tag>....
	bool GetXMLFirstTagValue(const std::string& xmlStr, const std::string& tag, std::string& tagValue)
	{
		string tagStart = "";
		tagStart.append("<");
		tagStart.append(tag);
		tagStart.append(">");

		string tagEnd = "";
		tagEnd.append("</");
		tagEnd.append(tag);
		tagEnd.append(">");

		bool b = false;
		string::size_type pos = xmlStr.find(tagStart);
		if (pos != string::npos)
		{
			pos += tagStart.length();
			string tempStr = xmlStr.substr(pos, xmlStr.length() - pos);
			pos = tempStr.find(tagEnd);
			tagValue = tempStr.substr(0, pos);
			b = true;
		}
		return b;
	}

#ifdef __AFXWIN_H__ // MFC requires
	//////////////////////////////////////////////////////////////////////////
	// 拆分字符串
	INT_PTR StrToK(CStringArray& sa, LPCTSTR str, LPCTSTR seps)
	{
		size_t nLen = (_tcslen(str)+1)*sizeof(TCHAR);
		TCHAR* szBuff = new TCHAR[nLen];
		ASSERT(szBuff);
		ZeroMemory(szBuff, nLen);
		StringCbCopy(szBuff, nLen, str);

		sa.RemoveAll();

		TCHAR *token = NULL;
		TCHAR *next_token = NULL;

#ifdef DEVELOPMENT_PLATFORM_VS
		token = _tcstok_s( szBuff, seps, &next_token );
		while( token != NULL )
		{
			sa.Add(token);
			token = _tcstok_s( NULL, seps, &next_token );
		}
#else // vc6
		token = _tcstok( szBuff, seps );
		while( token != NULL )
		{
			sa.Add(token);		
			token = _tcstok( NULL, seps );
		}
#endif

		delete []szBuff;
		return sa.GetSize();
	}

	INT_PTR StrToK_A(CStringArray& sa, LPCSTR str, LPCSTR seps)
	{
		CStringA strastr = str;
		CStringA straseps = seps;

		CString strtstr, strtseps;
		strtstr = strastr;
		strtseps = straseps;

		return StrToK(sa, strtstr, strtseps);
	}
#endif
	
	//////////////////////////////////////////////////////////////////////////
	// 字符串转大写
	std::string ToUpperA(std::string& strSrc)
	{
		string strDst;
#ifdef DEVELOPMENT_PLATFORM_VS
		strDst.clear();
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(strDst), ::toupper);
#else
		vector<CHAR> vecDst;
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(vecDst), ::toupper);
		vecDst.push_back('\0');
		strDst = vecDst.begin();
#endif
		return strDst;
	}

	wstring ToUpperW(wstring& strSrc)
	{
		wstring strDst;
#ifdef DEVELOPMENT_PLATFORM_VS
		strDst.clear();
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(strDst), ::toupper);
#else
		vector<WCHAR> vecDst;
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(vecDst), ::toupper);
		vecDst.push_back(L'\0');
		strDst = vecDst.begin();
#endif
		return strDst;
	}

	//////////////////////////////////////////////////////////////////////////
	// 字符串转小写
	std::string ToLowerA(std::string& strSrc)
	{
		string strDst;
#ifdef DEVELOPMENT_PLATFORM_VS
		strDst.clear();
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(strDst), ::tolower);
#else
		vector<CHAR> vecDst;
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(vecDst), ::tolower);
		vecDst.push_back('\0');
		strDst = vecDst.begin();
#endif
		return strDst;
	}

	wstring ToLowerW(wstring& strSrc)
	{
		wstring strDst;
#ifdef DEVELOPMENT_PLATFORM_VS
		strDst.clear();
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(strDst), ::tolower);
#else
		vector<WCHAR> vecDst;
		transform(strSrc.begin(), strSrc.end(), std::back_inserter(vecDst), ::tolower);
		vecDst.push_back(L'\0');
		strDst = vecDst.begin();
#endif
		return strDst;
	}

}

#ifdef __AFXWIN_H__ // MFC requires
namespace Msg
{
}

namespace Gdi
{
}
#endif

namespace ShellCode
{
}

#ifdef __AFXWIN_H__ // MFC requires
namespace Controls
{
}
#endif

#ifdef INCLUDE_MATCOM

#pragma chMSG(--------------------------------------------------)
#pragma chMSG(发布版需要v4501v.dll, ago4501.dll)
#pragma chMSG(--------------------------------------------------)

namespace Matcom
{
}
#endif

namespace Colors
{
}

namespace Search {
	namespace Sunday {
	}
}

namespace WMI {
}


#pragma data_seg()
