// otp.cpp : 定义 DLL 的初始化例程。
//

#include "stdafx.h"
#include "otp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: 如果此 DLL 相对于 MFC DLL 是动态链接的，
//		则从此 DLL 导出的任何调入
//		MFC 的函数必须将 AFX_MANAGE_STATE 宏添加到
//		该函数的最前面。
//
//		例如:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// 此处为普通函数体
//		}
//
//		此宏先于任何 MFC 调用
//		出现在每个函数中十分重要。这意味着
//		它必须作为函数中的第一个语句
//		出现，甚至先于所有对象变量声明，
//		这是因为它们的构造函数可能生成 MFC
//		DLL 调用。
//
//		有关其他详细信息，
//		请参阅 MFC 技术说明 33 和 58。
//


// CotpApp

BEGIN_MESSAGE_MAP(CotpApp, CWinApp)
END_MESSAGE_MAP()


// CotpApp 构造

CotpApp::CotpApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CotpApp 对象

CotpApp theApp;


// CotpApp 初始化

BOOL CotpApp::InitInstance()
{
	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
string leftpad(string str, int len, char pad) {
	string s = "";
	for (int i=(int)str.length(); i<len; i++)
	{
		s.append(1, pad);
	}
	s.append(str.c_str());
	return s;
}

int getCharPos(char ch)
{
	string base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	int pos = -1;
	for (int i=0; i<(int)base32chars.size(); i++)
	{
		if (base32chars.at(i) == ch)
		{
			pos = i;
			break;
		}
	}
	return pos;
}

static string bin2hex(string &strbin)
{
	string s;
	if (strbin == "0000")
	{
		s = "0";
	}
	if (strbin == "0001")
	{
		s = "1";
	}
	if (strbin == "0010")
	{
		s = "2";
	}
	if (strbin == "0011")
	{
		s = "3";
	}
	if (strbin == "0100")
	{
		s = "4";
	}
	if (strbin == "0101")
	{
		s = "5";
	}
	if (strbin == "0110")
	{
		s = "6";
	}
	if (strbin == "0111")
	{
		s = "7";
	}
	if (strbin == "1000")
	{
		s = "8";
	}
	if (strbin == "1001")
	{
		s = "9";
	}
	if (strbin == "1010")
	{
		s = "a";
	}
	if (strbin == "1011")
	{
		s = "b";
	}
	if (strbin == "1100")
	{
		s = "c";
	}
	if (strbin == "1101")
	{
		s = "d";
	}
	if (strbin == "1110")
	{
		s = "e";
	}
	if (strbin == "1111")
	{
		s = "f";
	}
	return s;
}

static string base32tohex(string &base32) {
	//GIYDERBTHA3DOMBZIM2DONJU
	//前4个字符位置
	//6 8 24 3
	//转成2进制5bit
	//00110 01000 11000 00011
	//每4位转16进制
	//0011 0010 0011 0000 0011
	//3    2    3    0    3  
	string locbase32 = String::ToUpperA(base32);

	string bits = "";
	string hex = "";

	for (int i = 0; i < (int)locbase32.length(); i++) {
		int val = getCharPos(locbase32.at(i));
		char buf1[100] = {0};
		itoa(val, buf1, 2);
		bits.append(leftpad(buf1, 5, '0'));

		//DbgPrintA("%d, %c, %d, %s, (%d)%s", i, locbase32.at(i), val, buf1, bits.length(), bits.c_str());
	}

	for (int i = 0; i + 4 <= (int)bits.length(); i += 4) {
		string chunk = bits.substr(i, 4);
		hex.append(bin2hex(chunk));

		//DbgPrintA("%d, %s, %s", i, chunk.c_str(), hex.c_str());
	}

	return hex;
}

string __stdcall get_otp(string &secrect)
{
	//////////////////////////////////////////////////////////////////////////
	string key1 = base32tohex(secrect);
	//DbgPrintA("%s\nkey1:%s", secrect.c_str(), key1.c_str());
	string key1hex = String::from_hex_string(key1);

	time_t tick;
	time(&tick);
	tick+=20;
	//DbgPrintA("ts:%d", tick);
	CHAR buff[100] = {0};
	StringCchPrintfA(buff, 100, "%016x", tick/30);
	string key2 = buff;
	//DbgPrintA("tick:%s", key2.c_str());

	// The data that we're going to hash
	string data = key2;
	string datahex = String::from_hex_string(data);

	// Be careful of the length of string with the choosen hash engine. SHA1 needed 20 characters.
	// Change the length accordingly with your choosen hash engine.     
	unsigned char result[20];
	unsigned int len = 20;

	HMAC_CTX ctx;
	HMAC_CTX_init(&ctx);

	// 接下来就是如何进行加密关键步骤
	// 这里的第三个参数还可以是 EVP_md5(), EVP_sha224, EVP_sha512, etc
	HMAC_Init_ex(&ctx, key1hex.c_str(), (int)key1hex.length(), EVP_sha1(), NULL);
	HMAC_Update(&ctx, (unsigned char*)datahex.data(), datahex.length());
	HMAC_Final(&ctx, (unsigned char*)result, &len);
	HMAC_CTX_cleanup(&ctx);

	string key3 = String::to_hex_string(result, 20);
	//DbgPrintA("hmac:%s", key3.c_str());

	typedef union tagHMACDATA {
		ULONG data;
		UCHAR bt[4];
	} HMACDATA;
	HMACDATA hd;

	hd.data = *(ULONG*)(result+(result[19]&0xf));
	ULONG data2 = MAKELONG(MAKEWORD(hd.bt[3],hd.bt[2]), MAKEWORD(hd.bt[1],hd.bt[0]));
	//DbgPrintA("data:%p,%p", hd.data, data2);
	data2 &= 0x7FFFFFFF;
	data2 %= 1000000;
	CStringA stra;
	stra.Format("%06u", data2);
	string str = stra;
	//DbgPrintA("结果:%s", str.c_str());
	return str;
}
